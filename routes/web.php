<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Auth::routes();

Route::group(['middleware' => ['auth'], 'prefix' => 'panel', 'namespace' => 'Admin'], function () {
    Route::name('admin.')->group(function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/users', 'AdminController@index')->name('users');
        Route::get('/users/create', 'AdminController@create')->name('user.create');
        Route::post('/users/store', 'AdminController@store')->name('user.store');
        Route::get('/users/edit/{id}', 'AdminController@edit')->name('user.edit');
        Route::put('/users/update/{id}', 'AdminController@update')->name('user.update');
        Route::delete('/users/delete/{id}', 'AdminController@destroy')->name('user.destroy');

        Route::get('companies', 'CompanyController@index')->name('companies');
        Route::get('companies/create', 'CompanyController@create')->name('company.create');
        Route::post('companies/store', 'CompanyController@store')->name('company.store');
        Route::get('companies/edit/{id}', 'CompanyController@edit')->name('company.edit');
        Route::put('companies/update/{id}', 'CompanyController@update')->name('company.update');
        Route::delete('companies/delete/{id}', 'CompanyController@destroy')->name('company.destroy');

        Route::get('/forums', 'ForumController@index')->name('forums');
        Route::get('/forum/create', 'ForumController@create')->name('forum.create');
        Route::post('/forum/store', 'ForumController@store')->name('forum.store');
        Route::get('/forum/edit/{id}', 'ForumController@edit')->name('forum.edit');
        Route::put('/forum/uptate/{id}', 'ForumController@update')->name('forum.update');
        Route::get('forum/{id}/role-playing', 'RolePlayController@indexRolePlay')->name('forum.create.roleplay');
        Route::post('forum/store/role-playing', 'RolePlayController@storeRolePlay')->name('forum.store.roleplay');
        Route::get('forum/role-play/edit/hour/{id}', 'RolePlayController@edit');
        Route::delete('forum/role-play/delete/hour/{id}', 'RolePlayController@destroy');
        Route::post('forum/role-play/hour/update', 'RolePlayController@update');
        Route::get('forum/{id}/interview', 'InterviewController@indexInterview')->name('forum.create.interview');
        Route::post('forum/store/interview', 'InterviewController@storeInterview')->name('forum.store.interview');
        Route::get('forum/interview/edit/hour/{id}', 'InterviewController@edit');
        Route::delete('forum/interview/delete/hour/{id}', 'InterviewController@destroy');
        Route::post('forum/interview/hour/update', 'InterviewController@update');
        Route::get('forum/{id}/aula-tic', 'AulaTicController@indexAulaTic')->name('forum.create.aulatic');
        Route::post('forum/store/aula-tic', 'AulaTicController@storeAulaTic')->name('forum.store.aulatic');
        Route::get('forum/aula-tic/edit/{id}', 'AulaTicController@edit')->name('forum.edit.aulatic');
        Route::put('forum/aula-tic/update/{id}', 'AulaTicController@update')->name('forum.update.aulatic');
        Route::delete('forum/aula-tic/delete/{id}', 'AulaTicController@destroy');
        Route::get('forum/{id}/work-shops', 'WorkShopController@indexWorkShop')->name('forum.create.workshop');
        Route::post('forum/store/work-shops', 'WorkShopController@storeWorkShop')->name('forum.store.workshop');
        Route::get('forum/competitors', 'CompetitorController@index')->name('forum.competitors');
        Route::get('forum/competitors/data', 'CompetitorController@data')->name('forum.competitors.data');
        Route::get('forum/data', 'ForumController@data')->name('forums.data');
        Route::get('forum/competitor/add', 'CompetitorController@create')->name('forum.competitor.create');
        Route::post('forum/competitor/store', 'CompetitorController@store')->name('forum.competitor.store');
        Route::put('forum/status/active/', 'ForumController@active')->name('forum.status.active');
        Route::put('forum/status/disable/', 'ForumController@disable')->name('forum.status.disable');
        Route::delete('forum/delete/{id}', 'ForumController@destroy')->name('forum.destroy');

        Route::get('/charts/ages', 'ChartController@chartAges');

        Route::prefix('cities')->group(function () {
            Route::get('/', 'CityController@index')->name('cities');
            Route::get('/create', 'CityController@create')->name('city.create');
            Route::post('/store', 'CityController@store')->name('city.store');
            Route::get('/edit/{id}', 'CityController@edit')->name('city.edit');
            Route::put('/update/{id}', 'CityController@update')->name('city.update');
            Route::delete('/delete/{id}', 'CityController@destroy')->name('city.update');
        });

        Route::prefix('hours')->group(function () {
            Route::get('/', 'HourController@index')->name('hours');
            Route::get('/create', 'HourController@create')->name('hour.create');
            Route::post('/store', 'HourController@store')->name('hour.store');
            Route::get('/edit/{id}', 'HourController@edit')->name('hour.edit');
            Route::put('/update/{id}', 'HourController@update')->name('hour.update');
        });

        Route::prefix('events')->group(function () {
            Route::name('events.')->group(function (){
                Route::prefix('role-plays')->group(function () {
                    Route::get('/', 'RolePlayController@index')->name('roleplays');
                    Route::get('/data', 'RolePlayController@data')->name('rolpelay.data');
                });
                Route::prefix('interviews')->group(function () {
                    Route::get('/', 'InterviewController@index')->name('interviews');
                    Route::get('/data', 'InterviewController@data')->name('interview.data');
                });
                Route::prefix('aula-tics')->group(function () {
                    Route::get('/', 'AulaTicController@index')->name('aulatics');
                    Route::get('/data', 'AulaTicController@data')->name('aulatic.data');
                });
                Route::prefix('work-shops')->group(function () {
                    Route::get('/', 'WorkShopController@index')->name('workshops');
                    Route::get('/data', 'WorkShopController@data')->name('workshop.data');
                });
            });
        });
    });
});



Route::get('/', 'WebController@index')->name('forum');
Route::post('/competitor/store', 'WebController@storeCompetitor')->name('store.competitor');
Route::prefix('events')->group(function () {
    Route::get('role-playing/{id?}', 'WebController@rolePlaying')->name('web.role-playing');
    Route::post('store/roleplay', 'WebController@storeRolePlaying')->name('web.store.roleplay');
    Route::get('entrevistas/{id?}', 'WebController@interview')->name('web.interview');
    Route::post('store/interview', 'WebController@storeInterview')->name('web.store.interview');
    Route::get('aula-tic/{id?}', 'WebController@aulatic')->name('web.aulatic');
    Route::post('store/aula-tic', 'WebController@storeAulaTic')->name('web.store.aulatic');
    Route::get('workshop/{id?}', 'WebController@workshop')->name('web.workshop');
    Route::post('store/work-shop', 'WebController@storeWorkshop')->name('web.store.workshop');
});

Route::get('get/hours/forum', 'WebController@getHoursByForum')->name('roleplay.get.hours.forum');
Route::get('get/company/interview', 'WebController@getCompanyByInterview')->name('interview.get.company.forum');
Route::get('get/hours/interview', 'WebController@getHoursByInterview')->name('interview.get.hours.forum');
Route::get('get/curses/aula-tic', 'WebController@getCursesByForum')->name('aulatic.get.curses.forum');
Route::get('get/workshops', 'WebController@getWorkShops')->name('forum.get.workshop');

Route::get('lang/{locale}', function ($locale){
    Session::put('locale', $locale);
    return redirect()->back();
});
