$(function() {
    $('.select-search').SumoSelect({
        search: true,
        searchText: 'Buscar...',
        placeholder: 'Seleccionar'
    });
    $('.select-multiple').SumoSelect({
        // okCancelInMulti: true,
        selectAll: true
    });
}(jQuery));
function mostrar(id) {
    if (id == "estudiante") {
        $(".centro-educativo").show();
        $(".empresa").hide();
        $(".otro").hide();
    }

    if (id == "profesor") {
        $(".centro-educativo").show();
        $("#empresa").hide();
        $("#otro").hide();
    }

    if (id == "empresa") {
        $(".centro-educativo").hide();
        $(".empresa").show();
        $(".otro").hide();
    }

    if (id == "otro") {
        $(".centro-educativo").hide();
        $(".empresa").hide();
        $(".otro").show();
    }
}

