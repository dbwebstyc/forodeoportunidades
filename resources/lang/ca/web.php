<?php

return [

    'titulo_busca_empleo' => 'Estàs en situació de desocupació? Cerques treball de forma activa?',
    'sub_titulo' => 'LABORA, Servei Valencià d\'Ocupació i Formació, en coordinació amb els pactes territorials d\'ocupació, organitza els fòrums d\'ocupació CONNECTA LABORA.',
    'form' => array(
        'nombre' => 'Nom',
        'apellido' => 'Cognoms',
        'email' => 'Correu electrònic',
        'phone' => 'Telèfon',
        'dni' => 'DNI',
        'select_foro' => ' Seleccionar fòrum',
        'registrarse_ahora' => 'Registrar-se ara',
        'hora' => 'Hora',
        'select_hora' => 'Seleccionar Hora',
        'select_workshop' => 'Seleccionar Taller',
        'select_curso' => 'Seleccionar Curso',
        'curso' => 'Aula TIC',
        'empresa' => 'Empresa',
        'select_empresa' => 'Seleccionar Empresa',
        'genero' => 'Genere',
        'select_genero' => 'Seleccionar Genere',
        'hombre' => 'Home',
        'mujer' => 'Dona',
        'no-binario' => 'No Binari',
        'select_edad' => 'Seleccionar Edat',
        'edad' => 'Edat',
        'anios' => 'anys',

    ),
    'events' => array(
        'proximos' => 'Els propers FÒRUMS CONNECTA LABORA són:',
    ),
    'seleccionar' => 'Seleccionar',
    'forum_title' => 'FÒRUM CONNECTA LABORA',
    'forum_description' => 'L’objectiu del fòrum <strong>CONNECTA LABORA</strong> és posar-te en contacte amb les empreses que ofereixen ocupació en la zona. Tindràs oportunitat d’entrevistar-te amb aquestes en el moment. Un grup de professionals estarà a la teua disposició per a informar-te sobre les millores tècniques de cerca d’ocupació, com ara gestionar les teues emocions o definir el teu projecte personal. També podràs accedir a l’Aula Tic en la qual el personal orientador t’assessorarà per a elaborar el teu currículum i donar-te les claus per a trobar el treball més adequat al teu perfil.',
    'menu' => array(
        'home' => 'Home',
        'roleplaying' => 'Simulacions d’entrevistes de treball',
        'workshop' => 'Tallers',
        'eventos' => 'Activitats de l\'Fòrum',
        'entrevistas_trabajo' => 'ENTREVISTES DE TREBALL',
        'entrevistas' => 'Entrevistes',
        'talleres' => 'Tallers',
        'aulatic' => 'Aulatic',
        'simulaciones' => 'Simulacions'
    ),
    'roleplaying' => 'Simulacions d’entrevistes de treball',
    'workshop' => 'TALLERS',
    'entrevistas_trabajo' => 'ENTREVISTES DE TREBALL',
    'forum_list' => array(
        'ya_te_registrastes' => 'Si ja et vas apuntar en el fòrum ara pots Registrar-te en:',
        'ubicacion' => 'Ubicació:',
        'forum_start' => 'Tots els fòrums comencen a les 9:30h',
        'forum_start' => 'Tots els fòrums comencen a les 9:30h',
    ),
    'months' => array(
        'noviembre' => 'Novembre,',
        'diciembre' => 'Desembre,',
    ),
    'recordatorio_foro_registro' => 'Recordeu que hauria d\'estar registrat al <a href="'.route('forum').'">Fòrum</a> per poder registrar-se a les activitats',
    'ver_itinerario' => 'Veure Programa',
    'cerrar' => 'Tancar',
    'cookies' => array(
        'message' => 'En la nostra web utilitzem cookies, tant pròpies com de tercers, per a millorar la teua experiència i mostrar-te la informació i publicitat adaptada als teus hàbits de navegació. Si continues navegant, entendrem que acceptes la nostra política de cookies.',
        'saber-mas' => 'Saber més',
        'boton' => 'Acceptar'
    ),
];
