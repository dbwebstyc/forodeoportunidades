<?php

return [
    'ya_registrado_forum' => 'Vosté ja s\'ha registrat en aquest Fòrum!',
    'registrado_en_forum' => 'Vosté ha sigut inscrit al FÒRUM, li enviem un correu.',
    'registro_forum_requerido' => 'Has d\'estar registrat prèviament en el Fòrum.',
    'ya_registrado_en_evento' => 'Vosté ja aquesta registrat en aquesta ACTIVITAT.',
    'registrado_en_evento' => 'Vosté ha sigut inscrit en aquesta ACTIVITAT.',
    'titulos' => array(
        'atencion' => 'Atenció!',
    )
];
