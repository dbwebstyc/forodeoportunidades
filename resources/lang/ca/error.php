<?php


return [
    'title_404' => 'Oops! Pàgina no oposada',
    'description_404' => 'No es va poder trobar la pàgina que estava buscant.',
    'volver_inicio' => 'Tornar a l\'Inici',
];
