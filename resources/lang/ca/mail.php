<?php


return [
    'inscripcion_title_forum' => 'Vostè ha estat inscrit en el Fòrum',
    'inscripcion_title' => 'La teua inscripció s\'ha realitzat amb èxit.',
    'foro' => 'Fòrum',
    'consultar' => 'Pots consultar el programa i l\'horari d\'activitats a la pàgina web',
    'ubicacion' => 'També trobaràs la ubicació del teu FÒRUM CONNECTA LABORA més pròxim. El fòrum comença a les 9.30 hores i finalitza a les 18.00 hores.',
    'a_traves_de' => 'A la web',
    'tambien' => 'també pots demanar cita per a les entrevistes de treball oferides per les empreses participants. Consulta-la i reserva la teua cita en l\'horari disponible.',
    'consultar_two' => 'Gràcies per la teua participació. En la web ',
    'actualizamos' => 's\'actualitza el programa i les ofertes d’ocupació.',
    'temari' => 'Temari',
    'curso' => 'Curs',
    'te_esperamos' => 'T\'esperem en el FÒRUM CONNECTA LABORA on podràs realitzar entrevistes de treball amb empreses oferents d’ocupació.',
    'te_invitamos' => 'Et convidem a participar en totes les activitats del programa. Per a això, has de reservar plaça en les activitats que desitges:',
    'simulaciones' => 'Simulacions d\'entrevistes de treball.',
    'aulatic' => 'Aula TIC.',
    'entrevistas' => 'Entrevistes.',
    'talleres' => 'Tallers per a millorar l\'ocupabilitat i la cerca d\'ocupació.',
    'recordatorio_qr' => 'RECORDA PORTAR AQUEST CODI QR PER A ACCEDIR Al FÒRUM. POT PORTAR-HO IMPRÉS EN PAPER O EN EL MÒBIL.',
    'puedes_consultar' => 'Pots consultar el programa i l\'horari d\'activitats en la pàgina web',
];
