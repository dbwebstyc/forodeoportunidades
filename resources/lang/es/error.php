<?php

return [
    'title_404' => 'Oops! Página no encontrada',
    'description_404' => 'No se pudo encontrar la página que estaba buscando.',
    'volver_inicio' => 'Volver al Inicio',
];
