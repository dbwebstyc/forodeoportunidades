<?php

return [
    'ya_registrado_forum' => 'Usted ya se ha registrado en este Foro!',
    'registrado_en_forum' => 'Usted ha sido inscrito al FORO, le enviamos un correo.',
    'registro_forum_requerido' => 'Debes estar registrado previamente en el Foro.',
    'ya_registrado_en_evento' => 'Usted ya esta registrado en esta ACTIVIDAD.',
    'registrado_en_evento' => 'Usted ha sido inscrito en esta ACTIVIDAD.',
    'titulos' => array(
        'atencion' => 'Atención!',
    )
];
