<?php


return [
    'inscripcion_title_forum' => 'Usted ha sido inscrito en el Foro.',
    'inscripcion_title' => 'Tu inscripción se ha realizado con éxito.',
    'foro' => 'Foro',
    'consultar' => 'Puedes consultar el programa y el horario de actividades en la página web',
    'ubicacion' => 'También encontrarás la ubicación de tu FÒRUM CONNECTA LABORA más cercano. El foro comienza a las 9:30 horas y finaliza a las 18:00 horas.',
    'a_traves_de' => 'A través de la web',
    'tambien' => 'también puedes pedir cita para las entrevistas de trabajo ofertadas por las empresas participantes.',
    'consultar_two' => 'Consulta en la web y reserva tu cita en el horario disponible. Gracias por tu participación. En la web',
    'actualizamos' => 'acualizamos el programa y las ofertas de empleo.',
    'temari' => 'Temario',
    'curso' => 'Curso',
    'te_esperamos' => 'Te esperamos en el FÒRUM CONNECTA LABORA donde podrás realizar entrevistas de trabajo con empresas ofertantes de empleo.',
    'te_invitamos' => 'Te invitamos a participar en todas las actividades del programa. Para ello, debes reservar plaza en las actividades que desees:',
    'simulaciones' => 'Simulaciones.',
    'aulatic' => 'Aula TIC.',
    'entrevistas' => 'Entrevistas.',
    'talleres' => 'Talleres.',
    'recordatorio_qr' => 'RECUERDA LLEVAR ESTE CÓDIGO QR PARA ACCEDER AL FORO<br>PUEDE LLEVARLO IMPRESO EN PAPER O EN EL MÓVIL.',
    'puedes_consultar' => 'Puedes consultar el programa y el horario de actividades en la página web',
];
