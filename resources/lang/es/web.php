<?php


return [
    'titulo_busca_empleo' => '¿Estás en situación de desempleo? ¿Buscas trabajo de forma activa?',
    'sub_titulo' => 'LABORA, Servicio Valenciano de Empleo y Formación, en coordinación con los Pactos Territoriales de Empleo, organiza los foros de empleo CONNECTA LABORA.',
    'form' => array(
      'nombre' => 'Nombre',
      'apellido' => 'Apellidos',
      'email' => 'Correo Electrónico',
      'phone' => 'Teléfono',
        'dni' => 'DNI',
        'select_foro' => 'Foro',
        'registrarse_ahora' => 'Registrarse ahora',
        'hora' => 'Hora',
        'select_hora' => 'Seleccionar Hora',
        'select_workshop' => 'Seleccionar Workshop',
        'select_curso' => 'Seleccionar Curso',
        'curso' => 'Curso',
        'empresa' => 'Empresa',
        'select_empresa' => 'Seleccionar Empresa',
        'genero' => 'Genero',
        'select_genero' => 'Seleccionar Genero',
        'hombre' => 'Hombre',
        'mujer' => 'Mujer',
        'no-binario' => 'No Binario',
        'select_edad' => 'Seleccionar Edad',
        'edad' => 'Edad',
        'anios' => 'años',

    ),
    'events' => array(
      'proximos' => 'Los próximos FOROS CONNECTA LABORA son:',
    ),
    'seleccionar' => 'Seleccionar',
    'forum_title' => 'FORO CONNECTA LABORA',
    'forum_description' => 'El objetivo del <strong>FORO CONNECTA LABORA</strong> es ponerte en contacto con las empresas que ofertan empleo en la zona. Tendrás oportunidad de entrevistarte con ellas en el momento. Un grupo de profesionales estará a tu disposición para informarte sobre las mejoras técnicas de búsqueda de empleo, cómo gestionar tus emociones o a definir tu proyecto personal. También podrás acceder al Aula Tic en la que el personal orientador te asesorará para elaborar tu currículum y darte las claves para encontrar el trabajo más adecuado a tu perfil.',
    'menu' => array(
        'home' => 'Home',
        'roleplaying' => 'SIMULACIONES DE ENTREVISTAS DE TRABAJO',
        'workshop' => 'TALLERES',
        'eventos' => 'Actividades del Foro',
        'entrevistas_trabajo' => 'ENTREVISTAS DE TRABAJO',
        'entrevistas' => 'Entrevistas',
        'talleres' => 'Talleres',
        'aulatic' => 'Aulatic',
        'simulaciones' => 'Simulaciones'
    ),
    'roleplaying' => 'SIMULACIONES DE ENTREVISTAS DE TRABAJO',
    'workshop' => 'TALLERES',
    'entrevistas_trabajo' => 'ENTREVISTAS DE TRABAJO',
    'forum_list' => array(
        'ya_te_registrastes' => 'Si ya te apuntaste en el foro ahora puedes Registrarte en:',
        'ubicacion' => 'Ubicación:',
        'forum_start' => 'Todos empiezan a las 9:30h',
    ),
    'months' => array(
        'noviembre' => 'Nobiembre,',
        'diciembre' => 'Diciembre,',
    ),
    'recordatorio_foro_registro' => 'Recuerde que debe estar registrado en el <a href="'.route('forum').'">Foro</a> para poder registrarse a las actividades',
    'ver_itinerario' => 'Ver Programa',
    'cerrar' => 'Cerrar',
    'cookies' => array(
        'message' => 'En nuestra web utilizamos cookies, tanto propias como de terceros, para mejorar tu experiencia y mostrarte
    la información y publicidad adaptada a tus hábitos de navegación. Si continúas navegando, entenderemos que
    aceptas nuestra política de cookies.',
        'saber-mas' => 'Saber más',
        'boton' => 'Aceptar'
    ),
];
