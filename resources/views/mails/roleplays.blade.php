@include('layouts.partials.mail.header')
<table cellpadding="0" cellspacing="0" class="es-content" align="center" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;table-layout:fixed !important;width:100%;">
    <tr style="border-collapse:collapse;">
        <td align="center" bgcolor="transparent" style="padding:0;Margin:0;background-color:transparent;">
            <table bgcolor="transparent" class="es-content-body" align="center" cellpadding="0" cellspacing="0" width="600" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-color:transparent;">
                <tr style="border-collapse:collapse;">
                    <td align="left" style="Margin:0;padding-bottom:15px;padding-top:30px;padding-left:30px;padding-right:30px;border-radius:10px 10px 0px 0px;background-color:#FFFFFF;background-position:left bottom;" bgcolor="#ffffff">
                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td width="540" align="center" valign="top" style="padding:0;Margin:0;">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;background-position:left bottom;">
                                        <tr style="border-collapse:collapse;">
                                            <td align="left" style="padding:0;Margin:0;"><h2 style="Margin:0;line-height:29px;mso-line-height-rule:exactly;font-family:tahoma, verdana, segoe, sans-serif;font-size:19px;font-style:normal;font-weight:bold;color:#BE0B34;text-align:center;">{!! trans('mail.inscripcion_title') !!}</h2></td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td align="center" height="30" bgcolor="transparent" style="padding:0;Margin:0;"></td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td align="left" style="padding:0;Margin:0;">
                                                <h2 style="Margin:0;line-height:21px;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;font-style:normal;font-weight:bold;color:#333333;text-align:center;">
                                                    {{trans('web.roleplaying')}}
                                                    <br><br>
                                                    Hora: <span style="color: #BE0B34">{{$competitor->RolePlay->getHourRolePlay()}}</span><br>
                                                    {{trans('mail.foro')}}: <span style="color: #BE0B34">
                                                        {{$competitor->RolePlay->forum->getForumDate()}},
                                                        {{$competitor->RolePlay->forum->city->getCity()}}
                                                        {{$competitor->RolePlay->forum->getLocation()}}</span>                                            </h2>
                                            </td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td align="center" height="19" bgcolor="transparent" style="padding:0;Margin:0;"></td>
                                        </tr>
                                        <tr style="border-collapse:collapse;">
                                            <td align="center" style="padding:0;Margin:0;padding-bottom:5px;"><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#131313;">
                                                    {!! trans('mail.consultar') !!}
                                                    <a target="_blank" href="http://www.connectalabora.es" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#BE0C34;">
                                                        www.connectalabora.es</a>.
                                                    {!! trans('mail.ubicacion') !!}
                                                </p>
                                                <p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#131313;"><br></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#131313;">
                                                    {{trans('mail.a_traves_de')}}
                                                    <a target="_blank" href="http://www.connectalabora.es" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#BE0C34;">
                                                        www.connectalabora.es</a>
                                                    {{trans('mail.tambien')}}
                                                </p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#131313;"><br></p><p style="Margin:0;-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-size:14px;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;line-height:21px;color:#131313;">
                                                    {!! trans('mail.consultar_two') !!}
                                                    <a target="_blank" href="http://www.connectalabora.es" style="-webkit-text-size-adjust:none;-ms-text-size-adjust:none;mso-line-height-rule:exactly;font-family:roboto, 'helvetica neue', helvetica, arial, sans-serif;font-size:14px;text-decoration:underline;color:#BE0C34;">
                                                        www.connectalabora.es </a>
                                                    {!! trans('mail.actualizamos') !!}</p></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr style="border-collapse:collapse;">
                    <td align="left" style="Margin:0;padding-top:5px;padding-bottom:5px;padding-left:30px;padding-right:30px;border-radius:0px 0px 10px 10px;background-position:left top;background-color:#FFFFFF;">
                        <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                            <tr style="border-collapse:collapse;">
                                <td width="540" align="center" valign="top" style="padding:0;Margin:0;">
                                    <table cellpadding="0" cellspacing="0" width="100%" style="mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse;border-spacing:0px;">
                                        <tr style="border-collapse:collapse;">
                                            <td align="center" style="padding:0;Margin:0;padding-top:20px;"><img class="adapt-img" src="http://www.connectalabora.es/wp-content/uploads/2019/11/logos-patrocinantes-800.png" alt style="display:block;border:0;outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;" width="540"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table></td>
    </tr>
</table>
@include('layouts.partials.mail.footer')
