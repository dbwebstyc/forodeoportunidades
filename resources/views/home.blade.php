@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Total de Edades</div>

                <div class="card-body">
{{--                    {!! $ages->container() !!}--}}
                    <canvas id="edades" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Total Participantes</div>

                <div class="card-body">
                    {!! $forum->container() !!}
{{--                    <canvas id="total-competitors" width="400" height="200"></canvas>--}}
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Total Participantes</div>

                <div class="card-body">
                                        <canvas id="generos" width="400" height="400"></canvas>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Total Participantes</div>

                <div class="card-body">
                    {!! $event->container() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        var cEdades = document.getElementById('edades').getContext('2d');
        var cGeneros = document.getElementById('generos').getContext('2d');
        var edades = new Chart(cEdades, {
            type: 'bar',
            data: {
                labels: {!! $ages->keys() !!},
                datasets: [{
                    label: 'Total de Edades',
                    data: {!! $ages->values() !!},
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                },
                maintainAspectRatio: false,
            }
        });
        var generos = new Chart(cGeneros, {
            type: 'bar',
            data: {
                labels: {!! $genre->keys() !!},
                datasets: [{
                    label: 'Total Generos',
                    data: {!! $genre->values() !!},
                    backgroundColor: [
                        'rgba(142, 68, 173, 0.2)',
                        'rgba(40, 116, 166, 0.2)',
                        'rgba(86, 101, 115, 0.2)'

                    ],
                    borderColor: [
                        'rgba(142, 68, 173, 1)',
                        'rgba(40, 116, 166, 1)',
                        'rgba(86, 101, 115, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            precision: 0
                        }
                    }]
                },
                maintainAspectRatio: false,
                responsive: true
            }
        });
    </script>
    {!! $forum->script() !!}
    {!! $event->script() !!}

    @endpush
