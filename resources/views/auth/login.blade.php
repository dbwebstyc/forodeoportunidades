@extends('layouts.login')

@section('content')
    <div class="d-flex align-items-center justify-content-center text-center">
        <div class="form-wrapper col-sm-12 col-md-6 offset-md-3">
            <div class="form-container my-4">
                <div class="register-logo text-center mb-4">
                    <img src="{{asset('images/logo.png')}}" alt="">
                </div>
                <div class="panel">
                    <div class="panel-header text-center mb-3">
                        <h3 class="fs-24">Iniciar Sesión</h3>
{{--                        <p class="text-muted text-center mb-0">Nice to see you! Please log in with your account.</p>--}}
                    </div>
                    <form class="register-form" action="{{route('login')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Correo de Usuario">
                            @error('email')
                                <div class="invalid-feedback text-left">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" id="pass" placeholder="Contraseña">
                            @error('password')
                            <div class="invalid-feedback text-left">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="remember" {{ old('remember') ? 'checked' : '' }}>
                            <label class="custom-control-label" for="remember">Recordar me </label>
                        </div>
                        <button type="submit" class="btn btn-success btn-block">Aceptar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection