@include('web.layouts.partials.header')
<body>
<!-- Preloader -->
<div class="preloader">
    <div class="loader"></div>
</div>
<!-- End Preloader -->

@include('web.layouts.partials.navbar')

@yield('content')

<!-- Start About Area -->
<section class="about-area ptb-120 bg-image">
    <div class="container">
        <div class="row h-100 align-items-center">
            <div class="col-md-8 offset-md-2">
                <div class="about-content">
                    <h2 class="text-center">{{trans('web.forum_title')}}</h2>
                    <p class="text-justify">{!! trans('web.forum_description') !!}</p>
                    <div class="signature">
                        <img src="{{asset('images/logos-patrocinantes.png')}}" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Area -->

@if(app()->getLocale() == 'es')
@include('web.layouts.partials.ofertas')
@endif
@include('web.layouts.partials.footer')

