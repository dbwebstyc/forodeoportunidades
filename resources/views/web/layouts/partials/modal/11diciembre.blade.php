<div class="modal fade" id="diciembre11" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <embed src="{{asset('docs/'. app()->getLocale() .'/ProgramaRequena.pdf')}}" frameborder="0" width="100%" height="600px">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{trans('web.cerrar')}}</button>
            </div>
        </div>
    </div>
</div>
