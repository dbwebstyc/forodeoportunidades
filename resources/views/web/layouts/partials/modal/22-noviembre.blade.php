<!-- Modal -->
<div class="modal fade"  id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
<table class="table table-bordered table-responsive">
  <h5 style="text-align:center; color:#ED1A3A;">Auditori  |  Aulari EPA/EOI  |  Hall<h5>
  <thead>
    <tr class="tr-rojo">
      <th scope="col">Horari</th>
      <th colspan="3">Saló d’actes</th>
      <th scope="col">Taller 1</th>
      <th scope="col">Taller 2</th>
      <th scope="col">Aula TIC</th>
      <th scope="col">Zona
d’entrevistes</th>
      <th scope="col">Stands
Informatius
</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="col">09:30</th>
      <td colspan="8"><p style="text-align:center;">Inauguració
- Enric Nomdedéu i Biosca, secretari autonòmic d’Ocupació i director general de LABORA
- Tania Baños, alcaldessa de la Vall d’Uixó i presidenta del Pacte Territorial por l’Ocupació de la Plana Baixa</p></td>
    </tr>
    <tr>
      <th scope="col">10:00</th>
      <td colspan="3">Sessió: “Les ocupacions del
futur”
- Nelson Marco, co-fundador i
DevOps Arker Labs
- Ignacio Llopis, Manaing
Director de IoTsens
- Tamara De Haro, responsa-
ble del departament de Capital
Humano de Nayar Systems
- Luis Miguel Batalla, tècnic
Creació , Consolidació i Creixe-
ment d’empreses, CEEI Castelló
- Héctor Llop, CEO de Pixelcom</td>
  <td>Taller:
“Eines per
a conéixer
les teues
fortaleces
professio-
nals”</td>
  <td>
Simulació
entrevista
de treball
  </td>
  <td>
Fes el teu
CV i carta de
presentació
  </td>
  <td>
Entrevistes de
treball
  </td>
  <td colspan="">
    Atenció al
públic
  </td>
    </tr>
    <tr class="tr-descanso-rojo">
      <th scope="col">10:45</th>
      <td colspan="8">Descans</td>
    </tr>
    <tr>
      <th scope="col">11:15</th>
      <td colspan="8">Zona Connecta</td>
    </tr>
    <tr>
      <th scope="col">12:00</th>
      <td colspan="3">Sessió “Serveis Labora”
Nicolás Sánchez Calduch</td>
      <td>Taller:
“Actituds i
pensaments
davant la
cerca d’ocu-
pació”</td>
      <td>Simulació
entrevista
de treball</td>
  <td>
    Construeix el
teu perfil de
GVAJobs/
Linkedin
  </td>
  <td>
    Entrevistes de
treball
  </td>
  <td colspan="">
    Atenció al
públic
  </td>
    </tr>
    <tr>
      <th scope="col">12:45</th>
      <td colspan="3">Taula redona: “Futur i
sostenibilitat del Turisme a
la Vall d’Uixó”
- Yolanda González, directora
comercial d’ASHOTUR (Asocia-
ción Provincial de Empresarios
de Hostelería y Turismo)
- Miguel Querol López, adjunt
a Presidència Marina d’Or
- Tania Baños, alcaldessa de
la Vall d’Uixó i presidenta del
Pacte Territorial per l’Ocupació
de la Plana Baixa</td>
      <td>Taller:
“Elabora
el teu pla
d’acció.
Formula
bé els teus
objectius”</td>
      <td>Simulació
entrevista
de treball</td>
  <td>Cerca
d’ocupació
per Internet</td>
  <td></td>
  <td></td>
    </tr>
    <tr>
      <th scope="col">13:15</th>
      <td colspan="5">Zona Connecta</td>
      <td>Ús lliurementoritzat</td>
      <td colspan="2">Zona Connecta</td>
    </tr>
    <tr class="tr-descanso-azul">
      <th scope="col">14:00</th>
      <td colspan="8">Descans</td>
    </tr>
    <tr>
      <th scope="col">16:00</th>
      <td colspan="3"></td>
      <td></td>
      <td></td>
      <td>Ús lliure
mentoritzat</td>
    <td>
      Entrevistes de treball
    </td>
    <td colspan="">
Atenció al
públic
    </td>
    </tr>
    <tr>
      <th scope="col">16:30</th>
      <td colspan="3">Taller videocurrículum</td>
      <td>Simulació
entrevista
de treball</td>
      <td>Simulació
entrevista
de treball</td>
  <td></td>
  <td colspan=""></td>
  <td colspan=""></td>
    </tr>
    <tr>
      <th scope="col">17:45</th>
      <td colspan="8">Clausura i lectura de conclusions</td>
    </tr>
  </tbody>
</table>
      </div>
    </div>
  </div>
</div>



