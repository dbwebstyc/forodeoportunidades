<h2 class="text-center">OFERTAS DE EMPLEO</h2>
<section class="faq-area">
    <div class="container">
        <div class="faq-accordion">
            <h2>Eurofirms: Almassora</h2>
            <ul class="accordion">
                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Técnico/a de desarrollo y ajustes (Inkjet)
                    </a>

                    <div class="accordion-content show" style="display: none;">
                        Seleccionamos para empresa del sector cerámico ubicada en la comarca de la Plana Baja, técnico/a de desarrollo y ajustes (Inkjet).
                        <br><strong>Funciones:</strong>
                        <ul>
                            <li>Ajustes de tonos y desarrollo de modelos con inkjet.</li>
                            <li>Verificar el ajuste del proyecto de diseño con las necesidades y requerimientos previamente planteados.</li>
                            <li>Crear bocetos y prototipos adaptando las técnicas decorativas al nuevo producto.</li>
                            <li>Adaptar los proyectos de diseño para su fabricación.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Ingeniería en Diseño Industrial, FP 2 ó CFGS en diseño de pavimentos y revestimientos cerámicos o experiencia equivalente.</li>
                            <li>Experiencia de al menos 2 años en las funciones descritas.</li>
                            <li>Conocimiento del proceso productivo cerámico o experiencia en colorificios.</li>
                            <li>Nivel alto en photoshop.</li>
                        </ul>
                        Horario de 08:30 a 17:30 h y de 11 a 20 h en semanas alternas de lunes a viernes.
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Técnico/a Electromecánico/a
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos para empresa del sector cerámico ubicada en la comarca de la Plana Baja, electromecánico/a de mantenimiento.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Montaje y revisión de sistemas y cuadros eléctricos.</li>
                            <li>Instalaciones y ajustes mecánicos de maquinaria.</li>
                            <li>Operaciones de mantenimiento preventivo y correctivo en la cadena de producción.</li>
                            <li>Reparaciones, modificaciones y acciones de mejora en equipos y maquinaria.</li>
                            <li>Recopilación de datos y elaboración de informes de incidencias y gestión de averías.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Profesional con formación técnica (mínimo FPI electricidad y/o mantenimiento industrial o similar).</li>
                            <li>Experiencia en las funciones descritas en entornos industriales.</li>
                            <li>Valorable haber trabajado en empresas del sector cerámico, porcelana, piedra,...</li>
                            <li>Valorable conocimiento de autómatas programables.</li>
                        </ul>
                        Horario: rodaturnos 14/7 (turnos de mañana, tarde y noche), posibilidad de realizar solo turno de mañana y tarde en 14/7
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Ingeniero/a químico junior
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos para empresa del sector cerámico ubicada en la comarca de la Plana Baja, ingeniero/a químico junior.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Tareas propias del área de producción y control de calidad de materias primas así como producto terminado (ensayos de abrasión método PEI, absorción de agua, manchas, resistencia mecánica, paso de agua, impacto,...).</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Titulados/as en ingeniería química o químicos con interés por el sectorcerámico y el control de calidad.</li>
                        </ul>
                        <strong>Horario:</strong> Jornada partida de lunes a viernes.
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Técnico/a en Diseño cerámico
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos técnico/a en Diseño cerámico para empresa del sector cerámico ubicada en la comarca de la Plana Baja
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>La tarea fundamental es traducir la estrategia de lanzamiento de producto en un lenguaje gráfico que define los nuevos productos.</li>
                            <li>Para ello trabajará en la realización de los bocetos y diseños de las nuevas colecciones. (Relieves, gráficas, colores, formatos, etc).</li>
                            <li>Obteniendo unas propuestas de diseño que el Comité de Producto revisará, analizará y aprobará para después iniciar su desarrollo.</li>
                            <li>Seguimiento del desarrollo y primera producción de los nuevos productos en rotocolor e inyección.</li>
                            <li>Seguimiento y solución de posibles problemas en la fabricación.</li>
                            <li>Colaboración con el dpto. de Marketing en la preparación de ferias.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Experiencia como diseñador/a en empresa cerámica o colorificio.</li>
                            <li>Manejo avanzado de Photoshop e ilustrator.</li>
                            <li>Manejo de Injeckt y rotocolor.</li>
                            <li>Se valorará conocimientos de fotografía.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Responsable de producción
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos Responsable de producción para empresa del sector textil ubicada en Vinaroz.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Gestión de los pedidos.</li>
                            <li>Coordinación del personal de fábrica.</li>
                            <li>Cumplimiento de los tiempos de fabricación.</li>
                            <li>Producción de material y de producto.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Formación profesional de Grado Superior en mecánica.</li>
                            <li>Valorable titulación técnica en Ingeniería Industrial.</li>
                            <li>De lunes a viernes, turno partido entre las 08:00h y las 18:00h</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Comercial
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos comercial para empresa del sector distribución a domicilio de agua natural mineral y bebidas refrescantes para trabajar en la provincia de Castellón.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Captación y gestión de nuevos clientes, cumpliendo los objetivos de venta marcados.</li>
                            <li>Realizar las acciones de marketing requeridas.</li>
                            <li>Realizar llamadas de seguimiento, puerta fría, etc.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Experiencia mínima de 6 meses en ventas en el mercado residencial o empresa.</li>
                            <li>Valorable experiencia en promociones, stands, etc..</li>
                            <li>Buscamos a una persona con habilidades de comunicación y capacidad de escucha, capacidad de trabajo en equipo y organizada.</li>
                            <li>Valorable residencia cercana al puesto de trabajo.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Administrativo/a de exportación
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos administrativo/a de exportación para trabajar en empresa del sector cerámico ubicada en Alcora
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Realización de créditos documentarios.</li>
                            <li>Gestión de pedidos, de cargas, facturación y gestión de cobro.</li>
                            <li>Gestión de clientes de mercados árabes.</li>
                            <li>Entre otras tareas propias del puesto de trabajo.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Experiencia en funciones propias en el departamento de exportación.</li>
                            <li>Buscamos a una persona responsable, organizada y dinámica.</li>
                            <li>Imprescindible Inglés Nivel C1.</li>
                            <li>Valorable Francés Nivel B2.</li>
                        </ul>
                        Horario de lunes a viernes jornada partida
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Técnico/a senior de automatización
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos técnico/a senior de automatización para empresa del sector industrial situada en Almazora
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Diseño de la automatización en procesos industriales.</li>
                            <li>Programación de PLC’s</li>
                            <li>Defensa ante clientes y control de proyectos.</li>
                            <li>Petición y revisión de ofertas a proveedores.</li>
                            <li>Diseño de máquinas y equipos industriales.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Estudios de Ingeniería o Ing. Técnica Industrial en Electrónica, Grado en Mecatrónica, Ingeniería de Telecomunicaciones o Formación Profesional Grado Superior en Sistemas de Regulación y Control.</li>
                            <li>Experiencia mínima de 4 años.</li>
                            <li>Domino de herramientas automatización Omron y/o Siemens.</li>
                            <li>Dominio de herramientas de diseño de Scada’s comerciales.</li>
                            <li>Conocimientos en pesaje.</li>
                            <li>Disponibilidad para viajar</li>
                        </ul>
                        <strong>Valorable:</strong>
                        <ul>
                            <li>Conocimientos de distintos lenguajes de programación de PLC’s.</li>
                            <li>Conocimientos lenguajes de programación C++, .net y visual.</li>
                            <li>Experiencia en diseño proyectos.</li>
                            <li>Conocimientos de inglés, francés.</li>
                        </ul>
                        Horario de lunes a viernes jornada partida
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Proyectista Senior
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos PROYECTISTA SENIOR para empresa perteneciente al sector industrial, situada en Almazora.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Diseño, defensa, dirección y seguimiento de proyectos.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Estudios de Ingeniería Industrial, Ingeniería Técnica Industrial Mecánica o Ingeniería Técnica en Diseño Industrial.</li>
                            <li>Amplia experiencia en diseño de instalaciones industriales.</li>
                            <li>Experiencia en dirección de personas.</li>
                            <li>Domino de AutoCAD.</li>
                            <li>Valorable conocimientos de programas de Diseño 3D, conocimientos de CYPE / CYPECAD, conocimientos de inglés.</li>
                        </ul>
                        Horario de lunes a viernes jornada partida
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Montador mecánico/a
                    </a>

                    <div class="accordion-content" style="">
                        Seleccionamos Montador mecánico/a para empresa del sector metal ubicada en Almazora.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>PLCs Omron (conexión on-line, interpretación de programa).</li>
                            <li>Variadores de frecuencia y drivers (ajuste parametrización).</li>
                            <li>Configuración bus AS-i.</li>
                            <li>Montaje y ajuste de maquinaria industrial.</li>
                            <li> Interpretación de planos mecánicos y neumáticos.</li>
                            <li>Soldadura básica.</li>
                            <li>Corte con radial.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Formación FP mecánica o similar.</li>
                            <li>Experiencia mínima de 1 año en las funciones descritas.</li>
                            <li>Persona dinámica, resolutiva y capaz de solucionar averías tanto vía telefónica com en casa del cliente</li>
                        </ul>
                        Horario de lunes a viernes jornada partida
                    </div>
                </li>

            </ul>
        </div>
    </div>
</section>
<section class="faq-area mt-5">
    <div class="container">
        <div class="faq-accordion">
            <h2>Torrecid: Almassora</h2>
            <ul class="accordion">
                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Técnico/a de laboratorio
                    </a>

                    <div class="accordion-content show" style="display: none;">
                        <strong>Descripción del puesto:</strong>

                        El grupo Torrecid busca incorporar una persona recién titulada en el grado en QUÍMICA o INGENIERÍA QUÍMICA para su sede en Alcora. Estamos buscando una persona con talento, innovadora, con vocación por las nuevas tecnologías y con ganas de crecer profesionalmente dentro de nuestra empresa. Desde un principio contará con formación especializada mediante un mentor, que se encargará de su formación y asesoramiento para que pueda aprender los procesos y las tecnologías que se utilizan en los departamentos de control de MP e I+D.
<br>
                        <strong>Funciones:</strong>
                        <ul>
                            <li>Manejo de técnicas de análisis de Instrumental: FRX, DRX,
                                AA, UV, ..</li>
                            <li>Manejo de equipos de investigación: MEB</li>
                            <li>Ofrecer soporte técnico a otros departamentos.</li>
                            <li>Técnicas de muestreo y control de calidad.</li>
                            <li>Desarrollo de nuevas técnicas.</li>
                            <li>Desarrollo y formulación de nuevos productos. Verificación de sus propiedades a nivel industrial.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Graduado/a en Química e Ingeniería Química.</li>
                            <li>Nivel medio de inglés.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        International Technical Assistance
                    </a>

                    <div class="accordion-content" style="">
                        ¿Quieres ser parte de nuestro proyecto? TORRECID es una empresa líder mundial en innovación que marca la moda y las tendencias en su sector, con un excelente ambiente de trabajo, en un entorno ético y relaciones basadas en la confianza, donde te formarás continuamente y podrás desarrollar tu carrera profesional, con un sistema de promoción interna 100% basado en la meritocracia.
<br>
                        <strong>Descripción del puesto:</strong>

                        Tras un periodo de formación técnica y un periodo de formación en gestión de clientes y equipos humanos, se responsabilizará de la gestión de un mercado extranjero. Deberán ser personas que quieran y puedan asumir responsabilidades en su mercado: gestión comercial de su cartera de
                        clientes, gestión de su equipo humano, establecimiento de objetivos y responsabilidad en su consecución.
<br>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Personas recién tituladas o con un máximo de dos años de experiencia laboral, con interés por desarrollarse en un ambiente internacional.</li>
                            <li>Personas extrovertidas y sociables.</li>
                            <li>Personas con iniciativa y capacidad de aprender rápido.</li>
                            <li>Pasión por viajar.</li>
                            <li>Idiomas: Nivel B2 en Inglés y/o Francés.</li>
                            <li>Titulación: Grado en cualquier Ingeniería o grado en Química.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Técnico Desarrollo
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Descripción del puesto:</strong>

                        Buscamos incorporar personas jóvenes, recién tituladas o con un máximo de dos años de experiencia laboral para desarrollarse en el área técnica. El trabajo implica frecuentes viajes al extranjero.
<br>
                        <strong>Funciones:</strong>

                        Buscamos una persona para apoyar las labores de asistencia internacional en la realización de controles cerámicos, pruebas de nuevos esmaltes y la realización, adaptación y puesta en marcha de nuevos diseños y efectos cerámicos en empresas clientes en todo el mundo.

                        El trabajo requiere frecuentes viajes por todo el mundo, por lo que se necesita que el candidato esté dispuesto a viajar más de 120 días al año.
<br>
                        <strong>¿Qué ofrecemos?</strong>
                        <ul>
                            <li>Plan de formación inicial y también a lo largo de toda la vida laboral.</li>
                            <li>Desarrollo de carrera profesional en función de méritos.</li>
                        </ul>
                        <strong>Requisitos:</strong>
                        <ul>
                            <li>Idiomas: Inglés o Francés nivel B1 o superior.</li>
                            <li>Manejo de Photoshop.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Instaladores fachadas ventiladas
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Descripción del puesto:</strong>

                        El grupo Torrecid para su empresa Wandegar busca incorporar instaladores especialistas en instalación de fachadas ventiladas con experiencia demostrable y
                        curso de 20 horas.
                        <br>
                        <img class="alignnone wp-image-383" src="{{asset('images/wandegar-logo.png')}}" alt="" width="215" height="55" />
                        <img class="alignnone size-full wp-image-382" src="{{asset('images/torrecid-group.png')}}" alt="" width="107" height="37" />
                    </div>
                </li>

            </ul>
        </div>
    </div>
</section>

<section class="faq-area mt-5">
    <div class="container">
        <div class="faq-accordion">
            <h2>Fundación Inserta</h2>
            <ul class="accordion">
                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Agente Vendedor Cupón de Once
                    </a>

                    <div class="accordion-content show" style="display: none;">
                        <strong>Resumen de la oferta: </strong>

                        Agente Vendedor Cupón de Once (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
<br>
                        <strong>Imprescindible estar en posesión de certificado de discapacidad y/o incapacidad laboral.</strong>
                        <br>
                        <strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>25
                        <br>
                        <strong>Lugar de trabajo: </strong>Castellón/ Castelló
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto: </strong>Entre 10.000 € y 14.000 €
                        <br>
                        <strong>Duración jornada: </strong>Entre 31 y 40 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Mañana y tarde
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li><strong>Venta de cupón en la zona asignada:</strong> venta de los distintos productos ofertados por once en la zona asignada.</li>
                        </ul>
                        <ul>
                            <li><strong>Gestión de la venta:</strong> Gestión correcta del producto, retirada, liquidación diaria, exposición y venta. Gestión correcta y venta a través del tpv. Prospección de mercado del área de influencia y contacto con colectivos.</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Se requiere movilidad para desplazarse por distintas zonas asignadas. Se requiere agilidad en el cálculo numérico y disponibilidad para trabajar fines de semana y/o festivos.</li>
                            <li>Para preinscribirse en esta oferta y que podamos valorar su solicitud debe acceder al portal como usuario candidato.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Ayudante de cocina en comedor de fábrica
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta:</strong>

                        Ayudante de cocina en comedor de fábrica (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong> </strong><strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>1
                        <br>
                        <strong>Lugar de trabajo: </strong>Castellón
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto:</strong> Informará la empresa
                        <br>
                        <strong>Duración jornada: </strong>Hasta 20 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Mediodía
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Trabajos auxiliares en la elaboración de productos. Tales como la preparación de hortalizas en crudo (limpiar, cortar, pelar y tornear). Llevar a cabo la limpieza de los utensilios y maquinaria de la cocina así como de determinadas operaciones de limpieza general de la cocina y enseres</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Horario de lunes a viernes de 12h- 16h</li>
                            <li>Ayudante de cocina en comedor de fábrica</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Cajero/a - Vendedor/a tienda de deporte Onda
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta:</strong>

                        Cajero/a- Vendedor/a tienda de deporte Onda (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong> </strong><strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>1
                        <br>
                        <strong>Lugar de trabajo: </strong>Onda
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto:</strong> Menos de 6.000€
                        <br>
                        <strong>Duración jornada: </strong>Hasta 20 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Turnos Rotativos
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Atención al cliente: atender e informar al cliente en la sección asignada</li>
                            <li>Cobro en caja: cobrar al cliente en caja</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Imprescindible practicar un deporte</li>
                            <li>Resumen de la oferta Cajero/a - Vendedor/a tienda de deporte onda</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Control de accesos Vila-real
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta</strong>:

                        Control de accesos Vila-real (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>1
                        <br>
                        <strong>Lugar de trabajo: </strong>Onda
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto:</strong> Entre 10.000€ y 14.000€
                        <br>
                        <strong>Duración jornada: </strong>Entre 31 y 40 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Turnos Rotativos
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Atención garita de entrada fábrica. Auxiliar de servicio/ control de accesos (el 95% de su jornada es sentado): atención e información a proveedores, registro base de datos de entradas y salidas, control pesaje camiones y control acceso visitas oficinas fábrica</li>
                        </ul>
                        <strong>Formación:</strong>
                        <ul>
                            <li>Microsoft Word: nivel medio</li>
                            <li>Microsoft Excel: nivel básico</li>
                            <li>Correo electrónico: nivel medio</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Carnet de conducir:</strong>
                        <ul>
                            <li>A, A1, A2, AM, B</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Dependiente/a Castellón
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta:</strong>

                        Dependiente/a Castellón (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>3
                        <br>
                        <strong>Lugar de trabajo: </strong>Castellón
                        <br>
                        <strong>Tipo contrato: </strong>Temporal de fomento de empleo para personas con discapacidad
                        <br>
                        <strong>Salario bruto: </strong>Informará la empresa
                        <br>
                        <strong>Duración jornada: </strong>Hasta 20 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Fines de semana
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Realización de las funciones y tareas derivadas de la atención al cliente en tienda: asesorar, atender y encaminar las demandas y preguntas del cliente.</li>
                            <li>Preparar prendas para hacerlas atractivas y accesibles,</li>
                            <li>Reponer la mercancía vendida y atender en los probadores.</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Buscamos personas apasionadas por el mundo de la moda, con disponibilidad para trabajar en horario de tardes y/o fines de semana. valorable experiencia en sector comercio, como dependienta/e.</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Encajadores/as almacén de frutas
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta: </strong>

                        Encajadores/as almacén de frutas (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>2
                        <br>
                        <strong>Lugar de trabajo: </strong>Vila-Real
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto: </strong>Entre 10.000 € y 14.000 €
                        <br>
                        <strong>Duración jornada: </strong>Entre 31 y 40 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Turnos rotativos
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Encajar y triar: encajar manualmente y clasificar las frutas</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Carnet de conducir:</strong>
                        <ul>
                            <li>A, AM, A1, A2, B</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Horario de lunes a viernes, turnos de mañana y tarde, se trabajan algunos sábados por la mañana</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Personal de limpieza
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta:</strong>

                        Personal de limpieza (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>2
                        <br>
                        <strong>Lugar de trabajo: </strong>Castellón
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto:</strong> Entre 6.000 € - 10.000 €
                        <br>
                        <strong>Duración jornada: </strong>Hasta 20 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Mañana
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Limpieza de oficinas: pasar mopa, limpieza polvo, fregado de suelos, limpieza de baños, etc</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Horario de lunes a viernes por las mañanas, 22 horas y media semanales</li>
                        </ul>
                    </div>
                </li>

                <li class="accordion-item">
                    <a class="accordion-title" href="javascript:void(0)">
                        <span><i class="icofont-plus"></i></span>
                        Operario/a Azulejera
                    </a>

                    <div class="accordion-content" style="">
                        <strong>Resumen de la oferta: </strong>

                        Operario/a Azulejera (Con Certificado de Discapacidad Igual o superior al 33% y/o Incapacidad Laboral)
                        <br>
                        <strong>IMPRESCINDIBLE ESTAR EN POSESIÓN DE CERTIFICADO DE DISCAPACIDAD Y/O INCAPACIDAD LABORAL.</strong>
                        <br>
                        <strong>Empresa: </strong>Inserta Empleo
                        <br>
                        <strong>Número de puestos: </strong>1
                        <br>
                        <strong>Lugar de trabajo: </strong>Vila-Real
                        <br>
                        <strong>Tipo contrato: </strong>Eventual por circunstancias de la producción
                        <br>
                        <strong>Salario bruto: </strong>Entre 18.000 € y 21.000 €
                        <br>
                        <strong>Duración jornada: </strong>Entre 31 y 40 horas
                        <br>
                        <strong>Turno/Jornada: </strong>Mañana y tarde
                        <br>
                        <strong>Funciones y tareas:</strong>
                        <ul>
                            <li>Operario/a azulejera: manejar en posición de pie la máquina de clasificación y composición de azulejos</li>
                        </ul>
                        <strong>Experiencia laboral:</strong>
                        <ul>
                            <li><strong>Tiempo experiencia: </strong>No se requiere</li>
                        </ul>
                        <strong>Carnet de conducir:</strong>
                        <ul>
                            <li>B</li>
                        </ul>
                        <strong>Otros aspectos a considerar:</strong>
                        <ul>
                            <li>Horario de lunes a viernes de mañana y tarde</li>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </div>
</section>
