
@include('web.layouts.partials.modal.5diciembre')
{{--@include('web.layouts.partials.modal.10diciembre')--}}
{{--@include('web.layouts.partials.modal.11diciembre')--}}

<div class="alert text-center cookiealert" role="alert">
    {{trans('web.cookies.message')}} <a href="#" target="_blank">{{trans('web.cookies.saber-mas')}}</a>

    <button type="button" class="btn btn-primary btn-sm acceptcookies" aria-label="Close">
        {{trans('web.cookies.boton')}}
    </button>
</div>
<!-- End Footer Area -->
<footer id="footer" class="default-footer">
    <div class="container">
        <p class="copyright-txt">Conecta Labora 2019. Todos los derechos reservados</p>
    </div>
</footer>
<!-- Modal -->

<!-- Back Top top -->
<div class="back-to-top">Top</div>
@include('web.layouts.assets.js')
</body>
