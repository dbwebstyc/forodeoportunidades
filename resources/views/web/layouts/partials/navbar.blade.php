
<!-- Start Navbar Area -->
<header id="header" class="header-area">
    <div class="elkevent-mobile-nav">
        <div class="logo">
            <a href="{{route('forum')}}"><img src="{{asset('images/logo-white.png')}}" alt="logo"></a>
        </div>
    </div>

    <div class="elkevent-nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light">
                <a class="navbar-brand" href="{{route('forum')}}">
                    <img src="{{asset('images/logo-white.png')}}" alt="logo" class="image-fluid">
                </a>

                <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a href="{{ url('lang/es') }}" class="nav-link">Castellano</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('lang/ca') }}" class="nav-link">Valenciano</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- End Navbar Area -->
