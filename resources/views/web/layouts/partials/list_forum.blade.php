                        <div class="header1 header-content">
                            <ul class="header-txt-list">
                                <li>
                                    <h4>{{trans('web.events.proximos')}}</h4>
                                </li>
                                @if($forums)
                                 @foreach($forums as $forum)
                                <li>
                                    <i class="fa fa-font-awesome"></i>
                                    <h4>{{$forum->city->getCity()}}
                                        @if(!empty($forum->program_es) || !empty($forum->program_ca))
                                        <a href="#" class="btn btn-white-connecta btn-sm-connecta" data-toggle="modal" data-target="#modalPDF"  onclick="showPDF('{{$forum->getProgram()}}')">{{trans('web.ver_itinerario')}}</a>
                                            @endif
                                    </h4>
                                    <p><b>{{$forum->getForumInfoAttribute()}}</b>, {{trans('web.forum_list.ubicacion')}} {{$forum->getLocation()}}</p>
                                    <p>
                                        <i>{{trans('web.forum_list.ya_te_registrastes')}}</i>
                                    </p>
                                    <p class="event-btn-">
                                        <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.interview', $forum->id)}}">{{trans('web.menu.entrevistas')}}</a>
                                        <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.workshop', $forum->id)}}">{{trans('web.menu.talleres')}}</a>
                                        <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.aulatic', $forum->id)}}">{{trans('web.menu.aulatic')}}</a>
                                        <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.role-playing', $forum->id)}}">{{trans('web.menu.simulaciones')}}</a>
                                    </p>
                                    <hr class=" event-hr">
                                </li>
                                    @endforeach
                                <li><h4>{{trans('web.forum_list.forum_start')}}</h4></li>
                                    @endif
                            </ul>
                        </div>

{{--                        <li>--}}
{{--                            <i class="fa fa-font-awesome"></i>--}}
{{--                            <h4>Ibi--}}
{{--                                <a href="#" class="btn btn-white-connecta btn-sm-connecta" data-toggle="modal" data-target="#diciembre10">{{trans('web.ver_itinerario')}}</a></h4>--}}
{{--                            <p><b>10 {{trans('web.months.diciembre')}}</b> {{trans('web.forum_list.ubicacion')}} Salvador Miró</p>--}}
{{--                            <p>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.interview')}}">{{trans('web.menu.entrevistas')}}</a>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.workshop')}}">{{trans('web.menu.talleres')}}</a>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.aulatic')}}">{{trans('web.menu.aulatic')}}</a>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.role-playing')}}">{{trans('web.menu.simulaciones')}}</a>--}}
{{--                            </p>--}}
{{--                            <hr class=" event-hr">--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <i class="fa fa-font-awesome"></i>--}}
{{--                            <h4>Requena--}}
{{--                                <a href="#" class="btn btn-white-connecta btn-sm-connecta" data-toggle="modal" data-target="#diciembre11">{{trans('web.ver_itinerario')}}</a>--}}
{{--                            </h4>--}}
{{--                            <p><b>11 {{trans('web.months.diciembre')}}</b> {{trans('web.forum_list.ubicacion')}} Recinte Firal</p>--}}
{{--                            <p>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.interview')}}">{{trans('web.menu.entrevistas')}}</a>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.workshop')}}">{{trans('web.menu.talleres')}}</a>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.aulatic')}}">{{trans('web.menu.aulatic')}}</a>--}}
{{--                                <a class="btn btn-blue-connecta btn-sm-connecta" href="{{route('web.role-playing')}}">{{trans('web.menu.simulaciones')}}</a>--}}
{{--                            </p>--}}
{{--                            <hr class=" event-hr">--}}
{{--                        </li>--}}
