
<!-- Bootstrap Min CSS -->
<link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
<!-- MeanMenu CSS -->
<link rel="stylesheet" href="{{asset('assets/css/meanmenu.css')}}">
<!-- IcoFont Min CSS -->
<link rel="stylesheet" href="{{asset('assets/css/icofont.min.css')}}">
<!-- Animate Min CSS -->
<link rel="stylesheet" href="{{asset('assets/css/animate.min.css')}}">
<!-- Magnific Min CSS -->
<link rel="stylesheet" href="{{asset('assets/css/magnific-popup.min.css')}}">
<!-- Odometer Min CSS -->
<link rel="stylesheet" href="{{asset('assets/css/odometer.min.css')}}">
<!-- Owl Carousel Min CSS -->
<link rel="stylesheet" href="{{asset('assets/css/owl.carousel.min.css')}}">
<!-- Style CSS -->
<link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
<!-- Responsive CSS -->
<link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">

<link rel="icon" type="image/png" href="{{asset('images/favicon.jpg')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha256-eZrrJcwDc/3uDhsdt61sL2oOBY362qM3lon1gyExkL0=" crossorigin="anonymous" />

{{--<link href="{{asset('css/sweetalert.css')}}" rel="stylesheet">--}}

{{--<link href="{{asset('css/toastr.css')}}" rel="stylesheet">--}}
