<!-- JQuery Min JS -->
<script src="{{asset('assets/js/jquery.min.js')}}"></script>
<!-- Popper Min JS -->
<script src="{{asset('assets/js/popper.min.js')}}"></script>
<!-- Bootstrap Min JS -->
<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
<!-- MeanMenu JS -->
<script src="{{asset('assets/js/jquery.meanmenu.js')}}"></script>
<!-- WoW Min JS -->
<script src="{{asset('assets/js/wow.min.js')}}"></script>
<!-- Magnific Popup Min JS -->
<script src="{{asset('assets/js/jquery.magnific-popup.min.js')}}"></script>
<!-- Appear Min JS -->
<script src="{{asset('assets/js/jquery.appear.min.js')}}"></script>
<!-- Lax JS -->
<script src="{{asset('assets/js/lax.js')}}"></script>
<!-- Odometer Min JS -->
<script src="{{asset('assets/js/odometer.min.js')}}"></script>
<!-- Main JS -->
<script src="{{asset('assets/js/main.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/cookieconsent@3/build/cookieconsent.min.js" data-cfasync="false"></script>
<script src="{{asset('assets/js/cookies.js')}}" type="text/javascript"></script>
@include('sweetalert::alert')
@stack('js')
<script type="text/javascript">
    function showPDF(url) {
        $('#modal-pdf').html('<embed src="'+ url +'" frameborder="0" width="100%" height="600px">');
        $('#modalPDF').show();
    }
</script>
