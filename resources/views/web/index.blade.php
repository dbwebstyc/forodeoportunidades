@include('web.layouts.partials.header')

@include('web.layouts.partials.navbar')

<!-- Start Main Banner -->
<div class="main-banner banner-with-form item-bg1">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="row align-items-center ">
                    <div class="col-md-12">
                        <div class="header-heading-title">
                            <h1>{{trans('web.titulo_busca_empleo')}}</h1>
                            <h4>{{trans('web.sub_titulo')}}</h4>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-12 ">
                        <div class="banner-form">
                            <form action="{{route('store.competitor')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="forum_id">{{trans('web.form.select_foro')}}</label>
                                    <select class="form-control" name="forum_id" required>
                                        <option value="">{{trans('web.seleccionar')}}</option>
                                        @foreach($forums as $forum)
                                            <option value="{{$forum->id}}">{{$forum->getForumDate()}} {{$forum->city->getCity()}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('web.form.nombre')}}</label>
                                    <input type="text" class="form-control" name="first_name" id="first_name" required>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('web.form.apellido')}}</label>
                                    <input type="text" class="form-control" name="last_name" id="last_name" required>
                                </div>
                                <div class="form-group">
                                    <label>{{trans('web.form.dni')}}</label>
                                    <input type="text" class="form-control" name="dni" id="dni" required>
                                </div>
                                <div class="form-group">
                                    <label for="age">{{trans('web.form.edad')}}</label>
                                    <select name="age" id="age" class="form-control">
                                        <option value="">{{trans('web.form.select_edad')}}</option>
                                        <option value="16-29">16-29 {{trans('web.form.anios')}}</option>
                                        <option value="30-44">30-44 {{trans('web.form.anios')}}</option>
                                        <option value="45-59">45-59 {{trans('web.form.anios')}}</option>
                                        <option value="+59">+59 {{trans('web.form.anios')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="genre">{{trans('web.form.genero')}}</label>
                                    <select name="genre" id="genre" class="form-control">
                                        <option value="">{{trans('web.form.select_genero')}}</option>
                                        <option value="M">{{trans('web.form.hombre')}}</option>
                                        <option value="F">{{trans('web.form.mujer')}}</option>
                                        <option value="NB">{{trans('web.form.no-binario')}}</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label>{{trans('web.form.email')}}</label>
                                    <input type="email" class="form-control" name="email" required>
                                </div>

                                <div class="form-group">
                                    <label>{{trans('web.form.phone')}}</label>
                                    <input type="text" class="form-control" name="phone" required>
                                </div>

                                <button type="submit" class="btn btn-primary">{{trans('web.form.registrarse_ahora')}}</button>
                            </form>
                        </div>

                    </div>
                    <div class="col-lg-7 col-md-12 offset-lg-1 offset-md-0">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        @endif
                            @include('web.layouts.partials.list_forum')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main Banner -->


<!-- Start About Area -->
<section class="about-area ptb-120 bg-image">
    <div class="container">
        <div class="row h-100 align-items-center">
            <div class="col-md-8 offset-md-2">
                <div class="about-content">
                    <h2 class="text-center">{{trans('web.forum_title')}}</h2>
                    <p class="text-justify">{!! trans('web.forum_description') !!}</p>
                    <div class="signature">
                        <img src="{{asset('images/logos-patrocinantes.png')}}" alt="image">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About Area -->

@if(app()->getLocale() == 'es')
@include('web.layouts.partials.ofertas')
@endif

@include('sweetalert::alert')
@include('web.layouts.partials.footer')
