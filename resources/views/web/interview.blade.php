@extends('web.layouts.web')

@section('content')
<!-- Start Main Banner -->
<div class="main-banner banner-with-form item-bg1">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="container">
                <div class="row align-items-center ">
                    <div class="col-md-12">
                        <div class="header-heading-title">
                            <h1>{{trans('web.entrevistas_trabajo')}}</h1>
                            <h4>{!! trans('web.recordatorio_foro_registro') !!}</h4>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-lg-4 col-md-12 ">
                        <div class="banner-form">
                            <form action="{{route('web.store.interview')}}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="forum">{{trans('web.form.select_foro')}}</label>
                                    <select class="form-control" name="forum_id" id="forum" required>
                                        <option value="">{{trans('web.seleccionar')}}</option>
                                        @foreach($forums as $forum)
                                            <option value="{{$forum->id}}" {{$forum->id == $forumId ? 'selected' : ''}}>{{$forum->forum_info}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="company">{{trans('web.form.empresa')}}</label>
                                    <select name="company" id="company" class="form-control" required>
                                        <option value="">{{trans('web.form.select_empresa')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="hour">{{trans('web.form.hora')}}</label>
                                    <select name="hour" id="hour" class="form-control" required>
                                        <option value="">{{trans('web.form.select_hora')}}</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="dni">{{trans('web.form.dni')}}</label>
                                    <input type="text" class="form-control" name="dni" id="dni" required>
                                </div>
                                <button type="submit" class="btn btn-primary">{{trans('web.form.registrarse_ahora')}}</button>
                            </form>
                        </div>

                    </div>
                    <div class="col-lg-7 col-md-12 offset-lg-1 offset-md-0">
                        @if ($errors->any())
                            <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                        @endif
                            @include('web.layouts.partials.list_forum')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Main Banner -->
@endsection
@push('js')
    <script type="text/javascript">
        $("#forum").change(function(){
            $('#company').html('');
            console.log($(this).val());
            $.ajax({
                url: "{{ route('interview.get.company.forum') }}?forum=" + $(this).val(),
                method: 'GET',
                success: function(data) {
                    console.log(data);
                    $('#company').html(data.html);
                }
            });
        });
        $("#company").change(function(){
            $('#hour').html('');
            var forum = $('#forum').val();
            console.log($(this).val());
            $.ajax({
                url: "{{ route('interview.get.hours.forum') }}?company=" + $(this).val() + '&forum='+ forum,
                method: 'GET',
                success: function(data) {
                    console.log(data);
                    $('#hour').html(data.html);
                }
            });
        });
    </script>
@endpush
