@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 offset-2">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h6 class="fs-17 font-weight-600 mb-0">Ciudades</h6>
                    </div>
                    <div class="text-right">
                        <div class="actions">
                            <a href="{{route('admin.city.create')}}" class="btn btn-success"> Nuevo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre Castellano</th>
                            <th>Nombre Valenciano</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cities as $key => $city)
                        <tr>
                            <th scope="row">{{$key += 1}}</th>
                            <td>{{$city->city_es}}</td>
                            <td>{{$city->city_ca}}</td>
                            <td>{!! $city->getStatus() !!}</td>
                            <td>
                                <a href="{{route('admin.city.edit', $city->id)}}" class="btn btn-info-soft btn-sm ">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <a href="#" onclick="deleteCity({{$city->id}})" class="btn btn-danger-soft btn-sm"><i class="fas fa-trash"></i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
@push('js')
    <script type="text/javascript">
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        function deleteCity(id)
        {
            Swal.fire({
                title: 'Eliminar Ciudad',
                text: "¿Esta seguro de eliminar la ciudad seleccionada?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    axios({
                        method: 'POST',
                        url: '/panel/cities/delete/' + id,
                        data: {_method: 'DELETE', _token: csrf_token}
                    }).then(function (response) {
                        if (response.data.success === true){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                            location.reload();
                        }else{
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });
                }
            })
        }

        </script>
    @endpush
