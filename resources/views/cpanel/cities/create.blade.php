@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-2">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Nueva Ciudad</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{route('admin.city.store')}}" method="POST" id="city-register">
                        @csrf
                        <div class="form-group row">
                            <label for="city_es" class="col-sm-3 col-form-label font-weight-600">Nombre Ciudad ES</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="city_es" id="city_es" value="{{old('city_es')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city_ca" class="col-sm-3 col-form-label font-weight-600">Nombre Ciudad VAL</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="city_ca" id="city_ca" value="{{old('city_ca')}}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label font-weight-600">Estatus</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="status" name="status">
                                    <option value="1" selected>Activo</option>
                                    <option value="0">Inactivo</option>
                                </select>
                            </div>
                        </div>

                        <button class="btn btn-success" type="submit">Guardar</button>
                        <a href="{{route('admin.cities')}}" class="btn btn-inverse">Voler</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
