<div class="row mt-3">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h6 class="fs-17 font-weight-600 mb-0">Work Shops</h6>
                    </div>
                    <div class="text-right">
                        <div class="actions">

                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <div class="table-responsive">
                    <table class="table table-stripe table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Temario ES</th>
                                <th>Temario VAL</th>
                                <th>Hora</th>
                                <th>Plazas</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($workshops as $key => $workshop)
                            <tr>
                                <td>{{$workshop->position}}</td>
                                <td>{{$workshop->temary_es}}</td>
                                <td>{{$workshop->temary_ca}}</td>
                                <td>{{$workshop->hour}}</td>
                                <td>{{$workshop->places}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
