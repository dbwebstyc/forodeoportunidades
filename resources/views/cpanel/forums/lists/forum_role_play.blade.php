    <div class="col-md-7">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h6 class="fs-17 font-weight-600 mb-0">Horas</h6>
                    </div>
                    <div class="text-right">
                        <div class="actions">

                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body text-center">
                <div class="table-responsive">
                    <table class="table table-stripe table-hover">
                        <thead>
                            <tr>
                                <th>Hora</th>
                                <th>Plazas</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($roleplays as $key => $hour)
                            <tr id="roleplay_id_{{ $hour->id }}">
                                <td>{{$hour->hour}}</td>
                                <td>{{$hour->places}}</td>
                                <td>
                                    <a href="#"  class="btn btn-info-soft btn-sm mr-2"><i class="fas fa-edit"></i></a>
{{--                                    <a href="javascript:void(0)" id="edit-roleplay" data-id="{{ $hour->id }}" class="btn btn-info-soft btn-sm mr-2"><i class="fas fa-edit"></i></a>--}}
{{--                                    <a href="javascript:void(0)" id="delete-roleplay" data-id="{{ $hour->id }}" class="btn btn-danger-soft btn-sm"><i class="fas fa-trash"></i></a>--}}
                                    <a href="#"  class="btn btn-danger-soft btn-sm"><i class="fas fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
