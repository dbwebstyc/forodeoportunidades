<div class="row mt-2">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h6 class="fs-17 font-weight-600 mb-0">Lista de cursos</h6>
                    </div>
                    <div class="text-right">
                        <div class="actions">
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-stripe table-hover">
                    <thead>
                    <tr>
                        <th>Nombre Curso ES</th>
                        <th>Nombre Curso VAL</th>
                        <th>Hora</th>
                        <th>Plazas</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($aula_tics as $key => $aula)
                    <tr id="aula_id_{{$aula->id}}">
                        <td>{{$aula->curse_es}}</td>
                        <td>{{$aula->curse_ca}}</td>
                        <td>{{$aula->hour}}</td>
                        <td>{{$aula->places}}</td>
                        <td>
                            <a href="#" class="btn btn-info-soft btn-sm"><i class="fas fa-edit"></i></a>
{{--                            <a href="{{route('admin.forum.edit.aulatic', $aula->id)}}" class="btn btn-info-soft btn-sm"><i class="fas fa-edit"></i></a>--}}
{{--                            <a href="#" onclick="deleteAulatic({{$aula->id}})" class="btn btn-danger-soft btn-sm"><i class="fas fa-trash"></i></a>--}}
                            <a href="#" class="btn btn-danger-soft btn-sm"><i class="fas fa-trash"></i></a>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
