@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Nuevo Foro</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forums')}}" class="btn btn-inverse btn-sm">
                                    Atras
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body text-center">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{route('admin.forum.store')}}" method="POST" id="forum-create" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="date" class="col-sm-4 col-form-label font-weight-600">Fecha</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="date" name="date" value="{{old('date')}}" min="{{Carbon::toDay()->toDateString()}}" id="date" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="city" class="col-sm-4 col-form-label font-weight-600">Ciudad</label>
                            <div class="col-sm-6">
                                <select name="city_id" id="city" class="select-search">
                                    @foreach($cities as $city)
                                        <option value="{{$city->id}}">{{$city->getCity()}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location_es" class="col-sm-4 col-form-label font-weight-600">Ubicación ES</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="location_es" value="{{old('location_es')}}" id="location_es" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="location_ca" class="col-sm-4 col-form-label font-weight-600">Ubicación VAL</label>
                            <div class="col-sm-6">
                                <input class="form-control" type="text" name="location_ca" value="{{old('location_ca')}}" id="location_ca" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="program_es" class="col-sm-4 col-form-label font-weight-600">Programa o Itinerario ES</label>
                            <div class="col-sm-6">
                                <input type="file" name="program_es" id="program_es" class="custom-input-file" data-multiple-caption="{count} files selected" />
                                <label for="program_es">
                                    <i class="fa fa-upload"></i>
                                    <span>Subir archivo…</span>
                                </label>
                            </div>
                        </div>
                        <div class="form-group row">

                            <label for="program_ca" class="col-sm-4 col-form-label font-weight-600">Programa o Itinerario VAL</label>
                            <div class="col-sm-6">
                                <input type="file" name="program_ca" id="program_ca" class="custom-input-file" />
                                <label for="program_ca">
                                    <i class="fa fa-upload"></i>
                                    <span>Subir archivo…</span>
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
