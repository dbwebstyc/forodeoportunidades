@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Forums</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forum.create')}}" class="btn btn-success btn-sm">
                                    Nuevo
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped" id="forum-list">
                            <thead>
                                <tr>
                                    <th>Fecha</th>
                                    <th>Ciudad</th>
                                    <th>Ubicación</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
@push('js')
    <script type="text/javascript">
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        function deleteForum(id)
        {
            Swal.fire({
                title: 'Eliminar Foro',
                text: "¿Esta seguro de eliminar este Foro?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    axios({
                        method: 'POST',
                        url: '/panel/forum/delete/' + id,
                        data: {_method: 'DELETE', _token: csrf_token}
                    }).then(function (response) {
                        if (response.data.success === true){
                            $("#forum-list").DataTable().ajax.reload();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });

                        }else{
                            $("#forum-list").DataTable().ajax.reload();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });
                }
            })
        }
        function activeForum(id)
        {
            Swal.fire({
                title: 'HABILITAR',
                text: "Desea cambiar el Estatus a este Foro",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    axios({
                        method: 'POST',
                        url: '{{route('admin.forum.status.active')}}?id=' + id,
                        data: {_method: 'PUT', _token: csrf_token}
                    }).then(function (response) {
                        if (response.data.success === true){
                            $("#forum-list").DataTable().ajax.reload();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }else{
                            $("#forum-list").DataTable().ajax.reload();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                        }
                    });
                }
            })
        }
        function disableForum(id)
        {
            Swal.fire({
                title: 'DESHABILITAR',
                text: "Desea cambiar el Estatus a este Foro",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    axios({
                        method: 'POST',
                        url: '{{route('admin.forum.status.disable')}}?id=' + id,
                        data: {_method: 'PUT', _token: csrf_token}
                    }).then(function (response) {
                        console.log(response);
                        if (response.data.success === true){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $("#forum-list").DataTable().ajax.reload();
                        }else{
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });
                            $("#forum-list").DataTable().ajax.reload();
                        }
                    });
                }
            })
        }

        $('#forum-list').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url: "{{ route('admin.forums.data') }}"
            },
            order: [[ 4, "desc" ]],
            columns: [
                {
                    data:'date',
                    name:'date'
                },
                {
                    data:'city',
                    name:'city'
                },
                {
                    data:'location',
                    name:'location'
                },
                {
                    data:'actions',
                    name:'actions',
                    orderable: false
                },
                {data: 'created_at', 'visible': false, name: 'created_at'}

            ]
        });

    </script>
    @endpush
