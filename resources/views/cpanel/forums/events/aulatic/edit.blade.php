@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Curso de Aula Tic</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forum.create.aulatic', $aulatic->forum_id)}}" class="btn btn-inverse btn-sm">
                                    Atras
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body ">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{route('admin.forum.update.aulatic', $aulatic->id)}}" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3" for="curse_es">Nombre Curso ES</label>
                            <input type="text" class="form-control col-md-6 mb-2 mr-sm-2" id="curse_es" value="{{$aulatic->curse_es}}" placeholder="Nombre Curso Español" name="curse_es" required>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3" for="curse_ca">Nombre Curso VAL</label>
                            <input type="text" class="form-control col-sm-6 mb-2 mr-sm-2" id="curse_ca" value="{{$aulatic->curse_ca}}" placeholder="Nombre Curso Valenciano" name="curse_ca" required>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3" for="hour">Hora</label>
                            <input type="time" class="form-control col-md-3 mb-2 mr-sm-2 hour" id="hour" name="hour" value="{{$aulatic->hour}}" required>

                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-sm-3" for="places">Plazas</label>
                            <input type="number" class="form-control col-md-2 mb-2 mr-sm-2" id="places" name="places"  value="{{$aulatic->places}}" required>

                        </div>
                        <button type="submit" class="btn btn-success mb-2">Actualizar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
