@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Curso de Aula Tic</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forums')}}" class="btn btn-inverse btn-sm">
                                    Atras
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body text-center">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                        <form class="form-inline" action="{{route('admin.forum.store.aulatic')}}" method="POST">
                            @csrf
                            <label class="sr-only" for="curse_es">Nombre Curso ES</label>
                            <input type="text" class="form-control col-md-4 mb-2 mr-sm-2" id="curse_es" placeholder="Nombre Curso Español" name="curse_es" required>
                            <label class="sr-only" for="curse_ca">Nombre Curso VAL</label>
                            <input type="text" class="form-control col-md-4 mb-2 mr-sm-2" id="curse_ca" placeholder="Nombre Curso Valenciano" name="curse_ca" required>

                            <label class="sr-only" for="hour">Hora</label>
                            <input type="time" class="form-control mb-2 mr-sm-2 hour" id="hour" name="hour" required>

                            <label class="sr-only" for="places">Plazas</label>
                            <input type="number" class="form-control col-md-2 mb-2 mr-sm-2" id="places" name="places" required>
                            <input type="hidden" name="forum_id" value="{{$forum->id}}">
                            <button type="submit" class="btn btn-success mb-2">Agregar</button>
                        </form>
                </div>
            </div>
        </div>
    </div>

    @includeIf('cpanel.forums.lists.forum_aulas', [$aula_tics])
@endsection
@push('js')
    <script type="text/javascript">
        var csrf_token = $('meta[name="csrf-token"]').attr('content');

        function deleteAulatic(id)
        {
            Swal.fire({
                title: 'Eliminar Foro',
                text: "¿Esta seguro de eliminar este Foro?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si',
                cancelButtonText: 'No'
            }).then((result) => {
                if (result.value) {
                    axios({
                        method: 'POST',
                        url: '/panel/forum/aula-tic/delete/' + id,
                        data: {_method: 'DELETE', _token: csrf_token}
                    }).then(function (response) {
                        if (response.data.success === true){
                            $('#aula_id_' + id).remove();
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            });

                        }else{
                            Swal.fire({
                                position: 'top-end',
                                icon: 'error',
                                title: response.data.message,
                                showConfirmButton: false,
                                timer: 1500
                            })
                        }
                    });
                }
            })
        }
        </script>
    @endpush
