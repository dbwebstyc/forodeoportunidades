@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-5">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Horas de Entrevista</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forums')}}" class="btn btn-inverse btn-sm">
                                    Atras
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body text-center">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{route('admin.forum.store.interview')}}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label for="company" class="col-sm-3 col-form-label font-weight-600">Compañia</label>
                            <div class="col-sm-5">
                                <select name="company_id" id="company" class="form-control select" required>
                                    <option value="">Seleccionar Empresa</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="hour" class="col-sm-3 col-form-label font-weight-600">Horas</label>
                            <div class="col-sm-5">
                                <select name="hour[]" multiple id="hour" class="select-multiple" required>
                                    @foreach($hours as $hour)
                                        <option value="{{$hour->getHour()}}">{{$hour->getHour()}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="places" class="col-sm-3 col-form-label font-weight-600">Plazas</label>
                            <div class="col-sm-5">
                                <input type="number" name="places" id="places" class="form-control" value="{{old('places')}}" min="0" required>
                            </div>
                        </div>
                        <input type="hidden" name="forum_id" value="{{$forum->id}}">
                        <button type="submit" class="btn btn-success btn-sm">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
        @includeIf('cpanel.forums.lists.forum_interview', [$interviews])
    </div>
    @include('layouts.modals.interview')
@endsection
@push('js')
    <script>
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            /* When click edit user */
            $('body').on('click', '#edit-interview', function () {
                var hour = $(this).data('id');
                $.get('/panel/forum/interview/edit/hour/' + hour, function (data) {
                    console.log(data.hour);
                    $('#interviewCrudModal').html("Editar Hora");
                    $('#btn-save').val("edit-interview");
                    $('#ajax-crud-modal').modal('show');
                    $('#interview_id').val(data.id);
                    $('#time').val(data.hour);
                    $('#plazas').val(data.places);
                })
            });
            //delete user login
            $('body').on('click', '#delete-interview', function () {
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer);
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                });
                var hour_id = $(this).data("id");
                Swal.fire({
                    title: 'Eliminar Hora',
                    text: "¿Esta seguro de eliminar esta Hora?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si',
                    cancelButtonText: 'No'
                }).then((result) => {
                    if (result.value) {
                        axios({
                            method: 'POST',
                            url: '/panel/forum/interview/delete/hour/' + hour_id,
                            data: {_method: 'DELETE'}
                        }).then(function (response) {
                            if (response.data.success === true){
                                $("#interview_id_" + hour_id).remove();
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'success',
                                    title: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                });

                            }else{
                                Swal.fire({
                                    position: 'top-end',
                                    icon: 'error',
                                    title: response.data.message,
                                    showConfirmButton: false,
                                    timer: 1500
                                })
                            }
                        });
                    }
                })
            });
        });

        if ($("#hourForm").length > 0) {
            $("#hourForm").validate({

                submitHandler: function(form) {

                    var actionType = $('#btn-save').val();
                    $('#btn-save').html('Actualizando..');

                    $.ajax({
                        data: $('#hourForm').serialize(),
                        url: "/panel/forum/interview/hour/update",
                        type: "POST",
                        dataType: 'json',
                        success: function (data) {
                            var hour = '<tr id="interview_id_' + data.id + '"><td>' + data.company_id + '</td><td>' + data.hour + '</td><td>' + data.places + '</td>';
                            hour += '<td><a href="javascript:void(0)" id="edit-interview" data-id="' + data.id + '" class="btn btn-info-soft btn-sm mr-2"><i class="fas fa-edit"></i></a>';
                            hour += '<a href="javascript:void(0)" id="delete-interview" data-id="' + data.id + '" class="btn btn-danger-soft btn-sm"><i class="fas fa-trash"></i></a></td></tr>';

                            $("#interview_id_" + data.id).replaceWith(hour);
                            $('#hourForm').trigger("reset");
                            $('#ajax-crud-modal').modal('hide');
                            $('#btn-save').html('Actualizar');
                            Toast.fire({
                                icon: 'success',
                                title: 'Hora actualizada'
                            })

                        },
                        error: function (data) {
                            console.log('Error:', data);
                            $('#btn-save').html('Actualizar');
                        }
                    });
                }
            })
        }

    </script>
@endpush
