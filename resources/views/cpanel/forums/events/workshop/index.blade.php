@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Work Shop</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forums')}}" class="btn btn-inverse btn-sm">
                                    Atras
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body text-center">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{route('admin.forum.store.workshop')}}" method="POST" class="form-inline">
                        @csrf
                            <label for="temary_es" class="sr-only font-weight-600">Temario ES</label>

                                <input type="text" name="temary_es" id="temary_es" class="form-control col-md-4 mb-2 mr-sm-2" value="{{old('temary_es')}}" placeholder="Nombre Taller ES" required>

                            <label for="temary_ca" class="sr-only font-weight-600">Temario VAL</label>

                                <input type="text" name="temary_ca" id="temary_ca" class="form-control col-md-4 mb-2 mr-sm-2" value="{{old('temary_ca')}}" placeholder="Nombre Taller VAL" required>

                            <label for="hour" class="sr-only font-weight-600">Hora</label>

                                <input type="time" name="hour" id="hour" class="form-control hour col-md-2 mb-2 mr-sm-2" value="{{old('hour')}}" placeholder="Hora" required>

                            <label for="places" class="sr-only font-weight-600">Plazas</label>

                                <input type="number" name="places" id="places" class="form-control col-md-2 mb-2 mr-sm-2" value="{{old('places')}}" placeholder="Plazas" required>

                        <input type="hidden" name="forum_id" value="{{$forum->id}}">
                        <button type="submit" class="btn btn-success btn-sm">Agregar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @includeIf('cpanel.forums.lists.forum_workshops', [$workshops])
@endsection
