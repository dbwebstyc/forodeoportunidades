@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Nuevo Foro</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forums')}}" class="btn btn-inverse btn-sm">
                                    Atras
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif
                    <form action="{{route('admin.forum.competitor.store')}}" method="POST" id="competitor-create" class="form-group">
                        @csrf
                        <div class="form-row mb-2">
                            <div class="col-md-4">
                                <label for="forum" class="font-weight-600">Foro</label>
                                <select name="forum_id" id="forum" class="form-control" required>
                                    <option value="">Seleccioanr Foro</option>
                                    @foreach($forums as $forum)
                                        <option value="{{$forum->id}}">{{$forum->city->getCity()}}</option>
                                        @endforeach
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="first_name" class="font-weight-600">Nombre</label>
                                <input type="text" class="form-control" name="first_name" id="first_name" value="{{old('first_name')}}" placeholder="Nombre Participante" required>
                            </div>
                            <div class="col-md-4">
                                <label for="last_name" class="font-weight-600">Apellidos</label>
                                <input type="text" class="form-control" name="last_name" id="last_name" value="{{old('last_name')}}" placeholder="Apellidos del Participante" required>
                            </div>
                        </div>
                        <div class="form-row mb-3">
                            <div class="col-md-4">
                                <label for="dni" class="font-weight-600">DNI</label>
                                <input type="text" class="form-control" name="dni" id="first_name" value="{{old('dni')}}" placeholder="Numero de Identificación" required>
                            </div>
                            <div class="col-md-4">
                                <label for="email" class="font-weight-600">Correo</label>
                                <input type="email" class="form-control" name="email" id="email" value="{{old('email')}}" placeholder="Correo Electrónico" required>
                            </div>
                            <div class="col-md-4">
                                <label for="phone" class="font-weight-600">Telefono</label>
                                <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone')}}" placeholder="Telefono Participante" required>
                            </div>

                        </div>
                        <button type="submit" class="btn btn-success ml-2">Enviar</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
