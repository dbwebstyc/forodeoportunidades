@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Participantes</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">
                                <a href="{{route('admin.forum.competitor.create')}}" class="btn btn-success btn-sm">Nuevo</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-inline" action="">
                                <label for="forum" class="mr-2">Foro</label>
                                <select class="form-control col-md-2 mb-2 mr-sm-2" name="forum" id="forum">
                                    <option value="all">Todos</option>
                                    @foreach($forums as $forum)
                                        <option value="{{$forum->id}}">{{$forum->forum_info}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-success mb-2" id="filter">Filtrar</button>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table display table-bordered table-striped table-hover" id="competitors-table">
                                    <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>DNI</th>
                                        <th>Edad</th>
                                        <th>Genero</th>
                                        <th>Correo</th>
                                        <th>Foro</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function(){

            fill_datatable();

            function fill_datatable(forum = 'all')
            {
                var dataTable = $('#competitors-table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('admin.forum.competitors.data') }}?forum=" + forum,
                        // data:{forum:forum}
                    },
                    columns: [
                        {
                            data:'code',
                            name:'code'
                        },
                        {
                            data:'first_name',
                            name:'first_name'
                        },
                        {
                            data:'last_name',
                            name:'last_name'
                        },
                        {
                            data:'dni',
                            name:'dni'
                        },
                        {
                            data:'age',
                            name:'age'
                        },
                        {
                            data:'genre',
                            name:'genre'
                        },
                        {
                            data:'email',
                            name:'email'
                        },
                        {
                            data:'forum',
                            name:'forum'
                        }
                    ],

                });
                new $.fn.dataTable.Buttons( dataTable, {
                    buttons: [
                        {
                            extend: 'excel',
                            className: 'btn-success mb-3',
                        },
                        {
                            extend: 'print',
                            className: 'btn-success mb-3'
                        },
                        {
                            extend: 'csv',
                            className: 'btn-success mb-3'
                        }
                    ]
                } );

                dataTable.buttons( 0, null ).container().prependTo(
                    dataTable.table().container()
                );
            }


            $('#filter').click(function(){
                var forum = $('#forum').val();

                if(forum !== '')
                {
                    $('#competitors-table').DataTable().destroy();
                    fill_datatable(forum);
                    $('#export')
                }
            });



        });
    </script>
    @endpush
