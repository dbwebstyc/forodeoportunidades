<form action="{{route('store.competitor')}}" method="POST">
@csrf
<div class="form-group">
    <label>{{trans('web.form.nombre')}}</label>
    <input type="text" class="form-control" name="first_name" id="first_name" required>
</div>
<div class="form-group">
    <label>{{trans('web.form.apellido')}}</label>
    <input type="text" class="form-control" name="last_name" id="last_name" required>
</div>
<div class="form-group">
    <label>{{trans('web.form.dni')}}</label>
    <input type="text" class="form-control" name="dni" id="dni" required>
</div>

<div class="form-group">
    <label>{{trans('web.form.email')}}</label>
    <input type="email" class="form-control" name="email" required>
</div>

<div class="form-group">
    <label>{{trans('web.form.phone')}}</label>
    <input type="text" class="form-control" name="phone" required>
</div>

<div class="form-group">
    <label for="forum_id">{{trans('web.form.select_foro')}}</label>
    <select class="form-control" name="forum_id" required>
        <option value="">{{trans('web.seleccionar')}}</option>
        @foreach($forums as $forum)
        <option value="{{$forum->id}}">{{$forum->forum_info}}</option>
            @endforeach
    </select>
</div>

<button type="submit" class="btn btn-primary">{{trans('web.form.registrarse_ahora')}}</button>
</form>