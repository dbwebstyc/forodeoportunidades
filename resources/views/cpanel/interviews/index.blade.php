@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Participantes en Entrevistas</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-3">
                        <div class="col-md-12">
                            <div class="form-inline" action="">
                                <label for="forum" class="mr-2">Foro</label>
                                <select class="form-control col-md-2 mb-2 mr-sm-2" name="forum" id="forum">
                                    <option value="all">Todos</option>
                                    @foreach($forums as $forum)
                                        <option value="{{$forum->id}}">{{$forum->forum_info}}</option>
                                    @endforeach
                                </select>
                                <label for="company" class="mr-2">Empresa</label>
                                <select class="form-control col-md-2 mb-2 mr-sm-2" name="company" id="company">
                                    <option value="all">Todas</option>
                                    @foreach($companies as $company)
                                        <option value="{{$company->id}}">{{$company->name}}</option>
                                    @endforeach
                                </select>
                                <button type="submit" class="btn btn-success mb-2" id="filter">Filtrar</button>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped" id="interview-competitors">
                            <thead>
                                <tr>
                                    <th>Code</th>
                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>DNI</th>
                                    <th>Hora</th>
                                    <th>Empresa</th>
                                    <th>Foro</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
@push('js')
    <script>
        $(document).ready(function(){

            fill_datatable();

            function fill_datatable(forum = 'all', company = 'all')
            {
                var dataTable = $('#interview-competitors').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax:{
                        url: "{{ route('admin.events.interview.data') }}?forum=" + forum + '&company=' + company,
                        // data:{forum:forum}
                    },
                    columns: [
                        {
                            data:'code',
                            name:'code'
                        },
                        {
                            data:'first_name',
                            name:'first_name'
                        },
                        {
                            data:'last_name',
                            name:'last_name'
                        },
                        {
                            data:'dni',
                            name:'dni'
                        },
                        {
                            data:'hour',
                            name:'hour'
                        },
                        {
                            data:'company',
                            name:'company'
                        },
                        {
                            data:'forum',
                            name:'forum'
                        }
                    ],

                });
                new $.fn.dataTable.Buttons( dataTable, {
                    buttons: [
                        {
                            extend: 'excel',
                            className: 'btn-success mb-3'
                        },
                        {
                            extend: 'print',
                            className: 'btn-success mb-3'
                        },
                        {
                            extend: 'csv',
                            className: 'btn-success mb-3'
                        }
                    ]
                } );

                dataTable.buttons( 0, null ).container().prependTo(
                    dataTable.table().container()
                );
            }


            $('#filter').click(function(){
                var forum = $('#forum').val();
                var company = $('#comapny').val();

                if(forum !== '' || company !== '')
                {
                    $('#interview-competitors').DataTable().destroy();
                    fill_datatable(forum, company);
                }
            });



        });
    </script>
@endpush
