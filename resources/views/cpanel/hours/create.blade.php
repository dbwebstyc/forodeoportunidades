@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6 offset-2">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div>
                            <h6 class="fs-17 font-weight-600 mb-0">Nueva Hora</h6>
                        </div>
                        <div class="text-right">
                            <div class="actions">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('admin.hour.store')}}" method="POST" id="city-register">
                        @csrf
                        <div class="form-group row">
                            <label for="hour_name" class="col-sm-3 col-form-label font-weight-600">Hora</label>
                            <div class="col-sm-6">
                                <input type="time" class="form-control hour" name="hour_name" id="hour_name"  required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="status" class="col-sm-3 col-form-label font-weight-600">Estatus</label>
                            <div class="col-sm-6">
                                <select class="form-control" id="status" name="status">
                                    <option value="1" {{old('status') == 1 ? 'selected' : '' }} selected>Activo</option>
                                    <option value="0" {{old('status') == 0 ? 'selected' : '' }}>Inactivo</option>
                                </select>
                            </div>
                        </div>

                        <button class="btn btn-success" type="submit">Guardar</button>
                        <a href="{{route('admin.hours')}}" class="btn btn-inverse">Voler</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
