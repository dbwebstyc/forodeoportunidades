@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-6 offset-2">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <div>
                        <h6 class="fs-17 font-weight-600 mb-0">Horas</h6>
                    </div>
                    <div class="text-right">
                        <div class="actions">
                            <a href="{{route('admin.hour.create')}}" class="btn btn-success"> Nuevo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Hora</th>
                            <th>Status</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($hours as $key => $hour)
                        <tr>
                            <th scope="row">{{$key += 1}}</th>
                            <td>{{$hour->getHour()}}</td>
                            <td>{!! $hour->getStatus() !!}</td>
                            <td>
                                <a href="{{route('admin.hour.edit', $hour->id)}}" class="btn btn-info-soft btn-sm ">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection
