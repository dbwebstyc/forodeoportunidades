@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-8 offset-md-2">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <h6 class="fs-17 font-weight-600 mb-0">
                        Usuarios Del Sistema
                    </h6>
                    <div class="text-right">
                        <div class="actions">
                            <a href="{{route('admin.user.create')}}" class="btn btn-success">Nuevo</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
               <div class="table-responsive">
                   <table class="table display table-bordered table-striped table-hover bg-white m-0 card-table">
                       <thead>
                        <tr>
                            <th>#</th>
                            <th>Nombre</th>
                            <th>Email</th>
                            <th>Acciones</th>
                        </tr>
                       </thead>
                       <tbody>
                       @foreach($users as $key => $user)
                        <tr>
                            <td>{{$key+=1}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-info-soft btn-sm mr-1"><i class="far fa-edit"></i></a>
                                <a href="#" onclick="deleteUser({{$user->id}})" class="btn btn-danger-soft btn-sm"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                           @endforeach
                       </tbody>
                   </table>
               </div>
            </div>
        </div>
    </div>
</div>
    @endsection
@push('js')
    <script type="text/javascript">
      function deleteUser(id)
      {
          var csrf_token = $('meta[name="csrf-token"]').attr('content');
          swal({
              title: "{{trans('alertas.title_eliminar')}}",
              text: "{{trans('alertas.text_eliminar_admin')}}",
              icon: "warning",
              buttons: true,
              dangerMode: true,
          }).then((deleteConfirm) => {
              if (deleteConfirm) {
                  axios({
                      method: 'POST',
                      url: '/cpanel/users/delete/' + id,
                      data: {_method: "DELETE", _token: csrf_token },
                  }).then(function (response) {
                      if (response.data.success === true) {
                          toastr.success(response.data.messsage)
                      }
                      if(response.data.error === true) {
                          toastr.error(response.data.messsage)
                      }
                  }).catch(function (error) {
                      if(error.data.error === true){
                          toastr.error(error.message)
                      }

                  });
              } else {
                  swal.close();
              }
          });
      }
    </script>
    @endpush
