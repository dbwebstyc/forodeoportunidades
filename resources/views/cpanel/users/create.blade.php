@extends('layouts.app')

@section('content')
    <form action="{{route('admin.user.store')}}" method="POST" id="user-create-form">
        @csrf
<div class="row">
    <div class="col-md-7 offset-md-2">
        <div class="card">
            <div class="card-header">
                <div class="d-flex justify-content-between align-items-center">
                    <h6 class="fs-17 font-weight-600 mb-0">Nuevo Usuario</h6>
                    <div class="text-right">
                        <div class="actions">
                            <button type="reset" class="action-item"><i class="ti-reload"></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-3 col-form-label font-weight-600">Usuario</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" placeholder="Nombre de Usuario" value="{{old('name')}}">
                            @error('name')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-md-3  col-form-label font-weight-600">Correo</label>
                        <div class="col-md-7">
                            <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}" placeholder="Correo Electronico">
                            @error('email')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="password" class="col-md-3 col-form-label font-weight-600">Contraseña</label>
                        <div class="col-md-6">
                            <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" placeholder="Contraseña de usuario">
                            @error('password')
                                <div class="invalid-feedback">
                                    {{$message}}
                                </div>
                            @enderror
                        </div>
                    </div>
                    <div class="text-center">
                            <button type="submit" class="btn btn-success"> <i class="fas fa-check"></i> Guardar</button>
                            <a href="{{route('admin.users')}}" class="btn btn-inverse"> <i class="fas fa-arrow-left"></i> Cancelar</a>
                    </div>

            </div>
        </div>
    </div>
</div>
    </form>
    @endsection
