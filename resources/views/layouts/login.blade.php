@include('layouts.partials.header')
<body class="bg-white">
<div id="app">
        @yield('content')

    @include('layouts.assets.js')
</div>
</body>

