<nav class="navbar navbar-expand-lg navbar-light bg-info">
    <a class="navbar-brand" href="#">Foro</a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="{{url('/')}}">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('web.role-playing')}}">Role Playing</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('web.workshop')}}">Workshop</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('web.aulatic')}}">Aula TIC</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{route('web.interview')}}">Entrevistas de Trabajo</a>
            </li>
            <li class="nav-item"><a class="nav-link" href="{{ url('lang/es') }}" ><i class="fas fa-language"></i> EN</a></li>

            <li class="nav-item"><a class="nav-link" href="{{ url('lang/ca') }}" ><i class="fas fa-language"></i> FR</a></li>
        </ul>
    </div>
</nav>
