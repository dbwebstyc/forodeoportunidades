<!-- Sidebar  -->
<nav class="sidebar sidebar-bunker">
    <div class="sidebar-header">
        <!--<a href="index.html" class="logo"><span>bd</span>task</a>-->
        <a href="{{route('admin.home')}}" class="logo"><img src="{{asset('images/logo.png')}}" alt=""></a>
    </div><!--/.sidebar header-->
    <div class="profile-element d-flex align-items-center flex-shrink-0">
        <div class="avatar online">
            <img src="{{asset('images/avatar/avatar5.png')}}" class="img-fluid rounded-circle" alt="">
        </div>
        <div class="profile-text">
            <h6 class="m-0">{{Auth::user()->name}}</h6>
            <span>{{Auth::user()->email}}</span>
        </div>
    </div><!--/.profile element-->
    <div class="sidebar-body">
        <nav class="sidebar-nav">
            <ul class="metismenu">
                <li class="nav-label">Main Menu</li>
                <li class="{{active(['admin.home'], 'mm-active')}}">
                    <a href="{{route('admin.home')}}">
                        <i class="typcn typcn-home-outline mr-2"></i>
                        Dashboard
                    </a>
                </li>
                <li class="{{active(['admin.users', 'admin.user.*'], 'mm-active')}}">
                    <a href="{{route('admin.users')}}">
                        <i class="fas fa-users mr-2"></i>
                        Usuarios
                    </a>
                </li>
                <li class="{{active(['admin.companies', 'admin.company.*'], 'mm-active')}}">
                    <a href="{{route('admin.companies')}}">
                        <i class="fas fa-user-tie mr-2"></i>
                        Compañias
                    </a>
                </li>
                <li class="{{active(['admin.forums', 'admin.forum.competitors'], 'mm-active')}}">
                    <a class="has-arrow material-ripple" href="#" aria-expanded="false">
                        <div class="material-ink animate" style="height: 220px; width: 220px; top: -89px; left: 89px;"></div>
                        <i class="fas fa-sitemap mr-2"></i>
                        Forums
                    </a>
                    <ul class="nav-second-level mm-collapse">
                        <li class="{{active(['admin.forums'], 'mm-active')}}"><a href="{{route('admin.forums')}}">Forums</a></li>
                        <li class="{{active(['admin.forum.competitors'], 'mm-active')}}"><a href="{{route('admin.forum.competitors')}}">Participantes</a></li>
                    </ul>
                </li>

                <li class="nav-label">Eventos</li>
                <li class="{{active(['admin.events.roleplays'], 'mm-active')}}">
                    <a href="{{route('admin.events.roleplays')}}">
                        <i class="far fa-clipboard mr-2"></i>
                        Simulaciones
                    </a>
                </li>
                <li class="{{active(['admin.events.interviews'], 'mm-active')}}">
                    <a href="{{route('admin.events.interviews')}}">
                        <i class="fas fa-pen-alt mr-2"></i>
                        Entrevistas
                    </a>
                </li>
                <li class="{{active(['admin.events.aulatics'], 'mm-active')}}">
                    <a href="{{route('admin.events.aulatics')}}">
                        <i class="fas fa-bullhorn mr-2"></i>
                        Aula TIC
                    </a>
                </li>
                <li class="{{active(['admin.events.workshops'], 'mm-active')}}">
                    <a href="{{route('admin.events.workshops')}}">
                        <i class="fas fa-toolbox mr-2"></i>
                        Talleres
                    </a>
                </li>
                <li class="nav-label">Otros</li>
                <li class="{{active(['admin.cities', 'admin.city.*'], 'mm-active')}}">
                    <a href="{{route('admin.cities')}}">
                        <i class="fas fa-city mr-2"></i>
                        Ciudades
                    </a>
                </li>
                <li class="{{active(['admin.hours', 'admin.hour.*'], 'mm-active')}}">
                    <a href="{{route('admin.hours')}}">
                        <i class="fas fa-clock mr-2"></i>
                        Horas
                    </a>
                </li>
            </ul>
        </nav>
    </div><!-- sidebar-body -->
</nav>
