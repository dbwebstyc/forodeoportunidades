<div class="modal fade" id="ajax-crud-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="interviewCrudModal"></h4>
            </div>
            <form id="hourForm" name="hourForm" class="form-horizontal">
                <div class="modal-body">
                    <input type="hidden" name="interview_id" id="interview_id">
                    <div class="form-group">
                        <label for="time" class="col-sm-2 control-label">Hora</label>
                        <div class="col-sm-12">
                            <input type="time" class="form-control" id="time" name="hour" value="" required="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Plazas</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" id="plazas" name="places" value="" required="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="btn-save" value="create">
                        Actualizar
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
