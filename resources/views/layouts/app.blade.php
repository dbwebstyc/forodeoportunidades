
@include('layouts.partials.header')
<body class="fixed">
        <!-- Page Loader -->
{{--        <div class="page-loader-wrapper">--}}
{{--            <div class="loader">--}}
{{--                <div class="preloader">--}}
{{--                    <div class="spinner-layer pl-green">--}}
{{--                        <div class="circle-clipper left">--}}
{{--                            <div class="circle"></div>--}}
{{--                        </div>--}}
{{--                        <div class="circle-clipper right">--}}
{{--                            <div class="circle"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <p>Please wait...</p>--}}
{{--            </div>--}}
{{--        </div>--}}
        <div class="wrapper">
            @include('layouts.partials.sidebar')
            <div class="content-wrapper">
                <div class="main-content">
                    @include('layouts.partials.navbar')
                    @include('layouts.partials.breadcrumbs')
                    <div class="body-content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
@include('layouts.partials.footer')
