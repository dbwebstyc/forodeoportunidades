
{{--<link rel="shortcut icon" href="{{asset('assets/img/favicon.png')}}">--}}
<!--Global Styles(used by all pages)-->
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('css/metisMenu.min.css')}}" rel="stylesheet">
<link href="{{asset('css/all.min.css')}}" rel="stylesheet">
<link href="{{asset('css/typicons/typicons.min.css')}}" rel="stylesheet">
<link href="{{asset('css/themify-icons/themify-icons.min.css')}}" rel="stylesheet">
{{--<link href="{{asset('css/sweetalert.css')}}" rel="stylesheet">--}}
<link href="{{asset('css/toastr.css')}}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />
<link href="{{asset('css/sumoselect.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('css/dataTables.bootstrap4.min.css')}}">
{{--@toastr_css--}}
<!--Third party Styles(used by this page)-->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<!--Start Your Custom Style Now-->
<link href="{{asset('css/style.css')}}" rel="stylesheet">

<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

