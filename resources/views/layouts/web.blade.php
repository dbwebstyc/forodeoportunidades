@include('layouts.partials.header')
<body class="fixed">
<div class="wrapper">
    <div class="content-wrapper">
        <div class="main-content">
            @include('layouts.partials.web.navbar')
            <div class="body-content">
                @yield('content')
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')
