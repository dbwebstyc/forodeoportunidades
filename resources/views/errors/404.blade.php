@include('web.layouts.partials.header')
    <!-- Start 404 Error -->
    <div class="error-area">
        <div class="d-table">
            <div class="d-table-cell">
                <h1>4<span>0</span><b>4</b>!</h1>
                <h3>{{trans('error.title_404')}}</h3>
                <p>{{trans('error.description_404')}}</p>
                <a href="{{route('forum')}}" class="btn btn-primary">{{trans('error.volver_inicio')}}</a>
            </div>
        </div>
    </div>
    <!-- End 404 Error -->
@include('.web.layouts.partials.footer')
