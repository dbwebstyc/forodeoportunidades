<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AulaTicCompetitor extends Model
{
    protected $guarded = [];

    public function aula_tic()
    {
        return $this->belongsTo(AulaTic::class);
    }

    public function forum_competitor()
    {
        return $this->belongsTo(ForumCompetitor::class);
    }

    // scope
    public function scopeCheckCompetitor($query, $dni, $forum)
    {
        return $query->where('forum_competitor_id', $dni)->where('forum_id', $forum);
    }
}
