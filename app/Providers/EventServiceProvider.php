<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        'App\Events\ForumRegistered' => [
            'App\Listeners\SendRegisteredMail',
        ],
        'App\Events\EventsRegistered' => [
            'App\Listeners\SendMailRolePlaying',
            'App\Listeners\SendMailWorkShop',
            'App\Listeners\SendMailAulaTic',
            'App\Listeners\SendMailInterview',
        ],
        'App\Events\NewCompanyRegistered' => [
            'App\Listeners\WelComeCompany',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
