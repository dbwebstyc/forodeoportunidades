<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkShopCompetitor extends Model
{
    protected $guarded = [];

    public function WorkShop()
    {
        return $this->belongsTo(WorkShop::class);
    }

    public function forum_competitor()
    {
        return $this->belongsTo(ForumCompetitor::class);
    }


    public function scopeCheckWorkShopCompetitor($query, $dni, $forum)
    {
        return $query->where('forum_competitor_id', $dni)->where('forum_id', $forum);
    }
}
