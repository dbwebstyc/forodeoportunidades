<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Hour extends Model
{
    protected $guarded = [];

    public function forumCityHour()
    {
        return $this->hasMany(ForumDateHour::class);
    }

    // Scopes

    public function scopeActive($query)
    {
        return $query->where('status' , 1);
    }

    public function getHour()
    {
        $hour = Carbon::parse($this->hour_name);
        return $hour->format('H:i');
    }

    public function getStatus()
    {
        if ($this->status){
            return '<span class="badge badge-success">Activo</span>';
        }
        return '<span class="badge badge-danger">Inactivo</span>';
    }

    public function scopesActive($query)
    {
        return $query->where('status', 1);
    }

}
