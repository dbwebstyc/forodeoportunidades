<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{
    protected $guarded = [];

    // relations
    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function InterviewCompetitor()
    {
        return $this->hasMany(InterviewCompetitor::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function company_hour_interview()
    {
        return $this->hasMany(CompanyHourInterview::class);
    }

    // getters
    public function getHourInterview()
    {
        $hour = Carbon::parse($this->hour);
        return $hour->format('H:i');
    }

}
