<?php

namespace App\Listeners;

use App\Events\EventsRegistered;
use App\Mail\MailForumRegister;
use App\Mail\MailRolePlay;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendMailRolePlaying
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventsRegistered  $event
     * @return void
     */
    public function handle(EventsRegistered $event)
    {
        $competitor = $event->competitor;
        $forumCompetitor = $event->forumCompetitor;
        Mail::to($forumCompetitor->email)->send(new MailRolePlay($competitor));
    }
}
