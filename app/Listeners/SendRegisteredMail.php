<?php

namespace App\Listeners;

use App\Events\ForumRegistered;
use App\Mail\MailForumRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class SendRegisteredMail
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForumRegistered  $event
     * @return void
     */
    public function handle(ForumRegistered $event)
    {
        $competitor = $event->competitor;
        Mail::to($competitor)->send(new MailForumRegister($competitor));
    }
}
