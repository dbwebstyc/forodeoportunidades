<?php

namespace App\Listeners;

use App\Events\EventsRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendMailWorkShop
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EventsRegistered  $event
     * @return void
     */
    public function handle(EventsRegistered $event)
    {
        //
    }
}
