<?php

namespace App\Listeners;

use App\Events\NewCompanyRegistered;
use App\Mail\MailWelcomeCompany;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

class WelComeCompany
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param  NewCompanyRegistered  $event
     * @return void
     */
    public function handle(NewCompanyRegistered $event)
    {
        $company = $event->company;

        Mail::to($company)->send(new MailWelcomeCompany($company));
    }
}
