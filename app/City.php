<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public function forum()
    {
        return $this->hasMany(Forum::class);
    }

    //Scopes

    public function ScopeActive($query)
    {
        return $query->where('status', 1);
    }

    // otros

    public function getStatus()
    {
        if ($this->status){
            return '<span class="badge badge-success">Activo</span>';
        }
        return '<span class="badge badge-danger">Activo</span>';
    }

    public function getCity()
    {
        $locale = app()->getLocale();
        $value = 'city_' . $locale;

        return $this->{$value};
    }
}
