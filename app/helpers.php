<?php

use App\ForumCompetitor;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use function foo\func;

function getHourFormat($value)
{
    $hour = Carbon::parse($value);

    return $hour->hour;
}

function getDateFormat($value)
{
    $date = Carbon::parse($value);

    return $date->day . ' ' . $date->monthName;
}

function getForoInfo($value1, $value2, $value3)
{
    $date = Carbon::parse($value1);

    return $date->day . ' ' . ucfirst($date->monthName) . ', ' . $value2. ', ' . $value3;
}

function generateCodeNumber() {
//    $number = mt_rand(1000000000, 9999999999); // better than rand()
//
//    // call the same function if the barcode exists already
//    if (codeNumberExists($number)) {
//        return  generateCodeNumber();
//    }

//    // otherwise, it's valid and can be used
    $last = DB::table('forum_competitors')->latest('id')->count();
    $sum = $last + 1;
    $sum = zero_fill($sum, 4);

    return $sum;
}

function codeNumberExists($number) {
    // query the database and return a boolean
    // for instance, it might look like this in Laravel
    return ForumCompetitor::whereCode($number)->exists();
}

function zero_fill ($valor, $long = 0)
{
    return str_pad($valor, $long, '0', STR_PAD_LEFT);
}

