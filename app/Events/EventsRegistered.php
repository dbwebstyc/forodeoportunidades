<?php

namespace App\Events;

use App\Forum;
use App\ForumCompetitor;
use App\RolePlayCompetitor;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EventsRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $forumCompetitor;
    public $competitor;

    /**
     * Create a new event instance.
     *
     * @param ForumCompetitor $forumCompetitor
     * @param RolePlayCompetitor $competitor
     */
    public function __construct(ForumCompetitor $forumCompetitor, RolePlayCompetitor $competitor)
    {
        $this->forumCompetitor = $forumCompetitor;
        $this->competitor = $competitor;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
