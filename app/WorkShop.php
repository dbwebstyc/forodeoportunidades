<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class WorkShop extends Model
{
    protected  $guarded = [];

    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function work_shop_competitor()
    {
        return $this->hasMany(WorkShopCompetitor::class);
    }

    public function getHour()
    {
        $hour = Carbon::parse($this->hour);
        return $hour->format('H:i');
    }

    public function getTemary()
    {
        $locale = app()->getLocale();
        $value = 'temary_' .$locale;
        return $this->{$value};
    }

    public function setPositionAttribute($forum)
    {
        $position = WorkShop::where('forum_id', $forum)->count();
        $sum = $position + 1;
        return  $this->attributes['position'] = 'Workshop ' . $sum;
    }
}
