<?php

namespace App\Mail;

use App\AulaTicCompetitor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisteredAulaTic extends Mailable
{
    use Queueable, SerializesModels;

    public $competitor;

    /**
     * Create a new message instance.
     *
     * @param AulaTicCompetitor $competitor
     */
    public function __construct(AulaTicCompetitor $competitor)
    {
        $this->competitor = $competitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.aulatic')->subject('Aula TIC: Foro Connecta Labora');
    }
}
