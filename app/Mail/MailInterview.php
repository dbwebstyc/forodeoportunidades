<?php

namespace App\Mail;

use App\InterviewCompetitor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailInterview extends Mailable
{
    use Queueable, SerializesModels;

    public $competitor;

    /**
     * Create a new message instance.
     *
     * @param InterviewCompetitor $competitor
     */
    public function __construct(InterviewCompetitor $competitor)
    {
        $this->competitor = $competitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.interview')->subject(trans('web.entrevistas_trabajo').': Foro Connecta Labora');
    }
}
