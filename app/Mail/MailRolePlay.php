<?php

namespace App\Mail;

use App\ForumCompetitor;
use App\RolePlayCompetitor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailRolePlay extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var ForumCompetitor
     */
    public $competitor;

    /**
     * Create a new message instance.
     *
     * @param RolePlayCompetitor $competitor
     */
    public function __construct(RolePlayCompetitor $competitor)
    {
        $this->competitor = $competitor;
//        $this->forumCompetitor = $forumCompetitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.roleplays')->subject(trans('web.roleplaying').': Foro Connecta Labora');
    }
}
