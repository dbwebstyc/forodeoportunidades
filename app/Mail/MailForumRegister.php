<?php

namespace App\Mail;

use App\ForumCompetitor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailForumRegister extends Mailable
{
    use Queueable, SerializesModels;

    public $competitor;

    /**
     * Create a new message instance.
     *
     * @param ForumCompetitor $competitor
     */
    public function __construct(ForumCompetitor $competitor)
    {
        $this->competitor = $competitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.forum')->subject('Registro: Foro Connecta Labora');
    }
}
