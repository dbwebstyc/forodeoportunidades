<?php

namespace App\Mail;

use App\WorkShopCompetitor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailWorkShop extends Mailable
{
    use Queueable, SerializesModels;

    public $competitor;

    /**
     * Create a new message instance.
     *
     * @param WorkShopCompetitor $competitor
     */
    public function __construct(WorkShopCompetitor $competitor)
    {
        $this->competitor = $competitor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.workshop')->subject(trans('web.workshop').': Foro Connecta Labora');
    }
}
