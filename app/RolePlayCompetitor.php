<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RolePlayCompetitor extends Model
{
    protected $guarded = [];

    // relations
    public function RolePlay()
    {
        return $this->belongsTo(RolePlay::class);
    }

    public function forum_competitor()
    {
        return $this->belongsTo(ForumCompetitor::class);
    }

    // scopes
    public function scopeCheckRolePlayCompetitor($query, $dni, $forum)
    {
        return $query->where('forum_competitor_id', $dni)->where('forum_id', $forum);
    }

}
