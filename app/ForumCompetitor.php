<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ForumCompetitor extends Model
{
    protected $fillable = [
         'type', 'code', 'first_name', 'last_name', 'dni', 'age', 'genre', 'email', 'phone', 'school',
        'company', 'position', 'number_employees', 'students_fp', 'event_interest', 'forum_id',
    ];


    protected $appends = ['full_name'];

    protected $dates = ['created_at', 'updated_at'];

    // relations
    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function roleplay_competitor()
    {
        return $this->hasMany(RolePlayCompetitor::class);
    }

    public function interview_competitor()
    {
        return $this->hasMany(InterviewCompetitor::class);
    }

    public function workshop_competitor()
    {
        return $this->hasMany(InterviewCompetitor::class);
    }


    // scopes
    public function scopeCheckForumCompetitor($query, $dni, $forum)
    {
        return $query->where('dni', $dni)->where('forum_id', $forum);
    }

    // getters
    public function getFullNameAttribute()
    {
        return $this->attributes['first_name'] . ' ' . $this->attributes['last_name'];
    }

    public function getEmpresa()
    {
        if (!empty($this->company)){
            return $this->company;
        }
        return 'N/A';
    }
    public function getCargo()
    {
        if (!empty($this->position)){
            return $this->position;
        }
        return 'N/A';
    }
    public function getNumberEmployees()
    {
        if (!empty($this->number_employees)){
            return $this->number_employees;
        }
        return 'N/A';
    }
    public function getInterest()
    {
        if (!empty($this->event_interest)){
            return $this->event_interest;
        }
        return 'N/A';
    }

    public function getGenre()
    {
        if ($this->genre == 'M')
        {
            return 'Hombre';
        }elseif ($this->genre == 'F'){
            return 'Mujer';
        }else{
            return 'No Binario';
        }
    }

}
