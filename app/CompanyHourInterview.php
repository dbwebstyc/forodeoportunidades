<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CompanyHourInterview extends Model
{
    protected $guarded = [];

    public function interview()
    {
        return $this->belongsTo(Interview::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }



    public function getHour()
    {
        $hour = Carbon::parse($this->hour);
        return $hour->format('H:i');
    }

}
