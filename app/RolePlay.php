<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class RolePlay extends Model
{
    protected $guarded = [];

    // relations
    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }
    public function RolePlayCompetitor()
    {
        return $this->hasMany(RolePlayCompetitor::class);
    }

    // getters
    public function getHourRolePlay()
    {
        $hour = Carbon::parse($this->hour);
        return $hour->format('H:i');
    }
}
