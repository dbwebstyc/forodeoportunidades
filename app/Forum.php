<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Forum extends Model
{
   use SoftDeletes;

    protected $guarded = [];

    protected $dates = ['date', 'created_at', 'update_at', 'delete_at'];

    protected $appends = [
      'forum_info'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getForumDate()
    {
        $date = Carbon::parse($this->date);
        return $date->day . '' . $date->monthName;
    }

    // relations
    public function forumcompetitor()
    {
        return $this->hasMany(ForumCompetitor::class);
    }

    public function role_play()
    {
        return $this->hasMany(RolePlay::class);
    }

    public function interviews()
    {
        return $this->hasMany(Interview::class);
    }

    public function workshop()
    {
        return $this->hasMany(WorkShop::class);
    }

    // scopes
    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function scopeCountCompetitors($query)
    {
        return $query->leftJoin('forum_competitors as competitor', 'forums.id', '=', 'competitor.forum_id')
            ->select(DB::raw("DATE_FORMAT(forums.date, '%d %b') as mes, COUNT(competitor.id) as total"))
            ->groupBy('forums.id')
            ->where('forums.status', '=', 1);
    }

    public function getForumInfoAttribute()
    {
        $date = Carbon::parse($this->attributes['date']);
        return $date->day . ' ' . ucfirst($date->monthName);
    }

    public function getLocation()
    {
        $locale = app()->getLocale();
        $value = 'location_' . $locale;
        return $this->{$value};
    }

    public function getProgram()
    {
        $locale = app()->getLocale();
        if ($locale == 'es')
        {
            return asset('uploads/docs/es') . '/' . $this->program_es;
        }
        return asset('uploads/docs/ca') . '/' . $this->program_ca;
    }

}

