<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class InterviewCompetitor extends Model
{
    protected $guarded = [];

    public function interview()
    {
        return $this->belongsTo(Interview::class);
    }

    public function forum_competitor()
    {
        return $this->belongsTo(ForumCompetitor::class);
    }

    // getters
    public function getHour()
    {
        $hour = Carbon::parse($this->hour);
        return $hour->format('H:i');
    }

    // scope
    public function scopeCheckCompetitor($query, $id, $company, $interviewId, $hour)
    {
//        return $query->where('forum_competitor_id', $dni)->where('forum_id', $forum);
        return $query->where('interview_competitors.forum_competitor_id', $id)
            ->where('hours.hour', '=', $hour)
            ->where('hours.company_id', '=', $company)
            ->where('hours.interview_id', '=', $interviewId)
//            ->join('forum_competitors as participante', 'interview_competitors.forum_comeptitor_id', '=', 'participante.id')
//            ->join('interviews', 'interview_competitors.interview_id', '=', 'interviews.id')
            ->join('company_hour_interviews as hours', 'hours.interview_id', '=', 'interview_competitors.interview_id');
    }
}
