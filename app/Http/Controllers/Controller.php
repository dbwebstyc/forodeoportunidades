<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function ReturnJson($dato = [], $activo = 0)
    {
        if ($activo == 0) {

            $dato['ruta'] = $this->getCurrentRoute();
            $dato['ruta_name'] = $this->getCurrentRouteName();
        }


        return json_encode($dato);

    }

    public function getCurrentRoute()
    {
        return Route::getFacadeRoot()->current()->uri();
    }

    public function getCurrentRouteName()
    {
        return Route::currentRouteName();
    }
}
