<?php

namespace App\Http\Controllers\Admin;

use App\AulaTic;
use App\AulaTicCompetitor;
use App\Charts\AgesChart;
use App\Charts\CompetitorChart;
use App\Charts\EventChart;
use App\Forum;
use App\ForumCompetitor;
use App\Http\Controllers\Controller;
use App\Interview;
use App\InterviewCompetitor;
use App\RolePlay;
use App\RolePlayCompetitor;
use App\User;
use App\WorkShop;
use App\WorkShopCompetitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function foo\func;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pageTitle = 'Administración';
        $pageIcon = 'typcn typcn-spiral';

        $collection = ForumCompetitor::all()->where('created_at', '>=', '2020-01-01')->groupBy('age');
        $ages = $collection->mapWithKeys(
            function ($item, $key){
                return [$key => count($item->values())];
            }
        );
        $ages = $ages->sortKeys();

        $forum = DB::table('forums')
            ->join('forum_competitors as competitor', 'forums.id', '=', 'competitor.forum_id')
            ->select(DB::raw("DATE_FORMAT(forums.date, '%d %b') as mes, COUNT(competitor.id) as total"))
            ->groupBy('mes')
            ->orderBy('mes', 'DESC')
            ->where('forums.status', '=', 1)
            ->where('forums.created_at', '>=', '2020-01-01')
            ->get()->toArray();
        $mes = array_column($forum, 'mes');
        $total = array_column($forum, 'total');

        $forum = new CompetitorChart();
        $forum->labels($mes);
        $forum->dataset('Total de Registros', 'bar', $total)->options([
            'backgroundColor' => [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
            ],
            'responsive' => true,
            'maintainAspectRatio' => false,

        ]);

        $collection = ForumCompetitor::all()->where('created_at', '>=', '2020-01-01')->groupBy('genre');
        $genre = $collection->mapWithKeys(
            function ($item, $key){
                return [$key => count($item->values())];
            }
        );
        $genre = $genre->sortKeys();

        $roleplay = RolePlayCompetitor::where('created_at', '>=', '2020-01-01')->count();
        $aulatic = AulaTicCompetitor::where('created_at', '>=', '2020-01-01')->count();
        $workshop = WorkShopCompetitor::where('created_at', '>=', '2020-01-01')->count();
        $interview = -InterviewCompetitor::where('created_at', '>=', '2020-01-01')->count();

        $event = new EventChart;
        $event->labels(['Simulaciones', 'Entrevistas', 'Aula TIC', 'Talleres']);
        $event->dataset('Actividades', 'bar', [$roleplay, $aulatic, $workshop, $interview])->options([
            'backgroundColor' => [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)'
            ],
            'responsive' => true,
            'maintainAspectRatio' => false
        ]);

        return view('home', compact(
            'pageTitle', 'pageIcon', 'ages', 'forum', 'genre', 'event'
        ));
    }
}
