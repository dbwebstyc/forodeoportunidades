<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Http\Controllers\Controller;
use App\Http\Requests\CityStoreRequest;
use App\Http\Requests\CityUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CityController extends Controller
{
    public function index()
    {
        $pageTitle = 'Ciudades';
        $pageIcon = 'fas fa-city';
        $cities = City::all();

        return view('cpanel.cities.index', compact('pageIcon','pageTitle', 'cities'));
    }

    public function create()
    {
        $pageTitle = 'Nueva Ciudad';
        $pageIcon = 'fas fa-city';

        return view('cpanel.cities.create', compact('pageTitle', 'pageIcon'));
    }

    public function store(CityStoreRequest $request)
    {
        $city = new City($request->all());
        $city->save();

        toastSuccess('Ciudad Creada');
        return Redirect::route('admin.cities');
    }

    public function edit($id)
    {
        $pageTitle = 'Editar Ciudad';
        $pageIcon = 'fas fa-city';
        $city = City::find($id);

        return view('cpanel.cities.edit', compact(
            'pageIcon', 'pageTitle', 'city'
        ));
    }

    public function update(CityUpdateRequest $request, $id)
    {
        $city = City::find($id);
        $city->update($request->all());

        toastSuccess('Ciudad actualizada');
        return Redirect::route('admin.cities');
    }

    public function destroy($id)
    {
        $data = [];
        $data['success'] = false;

        $city = City::find($id);
        $city->delete();
        if (empty($city))
        {
            $data['message'] = 'Error al Eliminar la ciudad';
            return $this->ReturnJson($data);
        }

        $data['success'] = true;
        $data['message'] = 'Ciudad Eliminada';

        return $this->ReturnJson($data);


    }
}
