<?php

namespace App\Http\Controllers\Admin;

use App\AulaTic;
use App\AulaTicCompetitor;
use App\Forum;
use App\RolePlayCompetitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class AulaTicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Participantes de Aula TIC';
        $pageIcon = 'fas fa-user';
        $forums = Forum::all();
        $aulas = AulaTic::all();
        $competitors = AulaTicCompetitor::all();

        return  view('cpanel.aulatics.index', compact(
            'pageIcon', 'pageTitle', 'competitors',
            'forums', 'aulas'
        ));
    }

    public function indexAulaTic($id)
    {
        $pageTitle = 'Aula TIC';
        $pageIcon = 'fas fa-bullhorn';
        $forum = Forum::find($id);
        $aula_tics = AulaTic::where('forum_id', $id)->get();

        return view('cpanel.forums.events.aulatic.index', compact(
            'pageTitle', 'pageIcon', 'forum', 'aula_tics'
        ));
    }

    public function storeAulaTic(Request $request)
    {

        $find = AulaTic::where('hour', $request->hour)->first();
        if (!empty($find)){
            toastWarning('Horas Duplicadas');
            return Redirect::back();
        }

        $aula = new AulaTic($request->all());
        $aula->save();

        toastSuccess('Curso Asignado');
        return Redirect::back();

    }

    public function data(Request $request)
    {

        $competitors = AulaTicCompetitor::join('aula_tics', 'aula_tic_competitors.aula_tic_id', '=', 'aula_tics.id');
        if ($request->forum != 'all' && $request->forum != '')
        {
            $competitors = $competitors->where('aula_tics.forum_id', $request->forum);
        }
        if ($request->curse != 'all' && $request->curse != '')
        {
            $competitors = $competitors->where('aula_tics.curse', $request->cuse);
        }

        $competitors = $competitors->get();

        return DataTables::of($competitors)
            ->editColumn('code', function (AulaTicCompetitor $competitor) {
                return $competitor->forum_competitor->code;
            })
            ->editColumn('first_name', function (AulaTicCompetitor $competitor) {
                return $competitor->forum_competitor->first_name;
            })
            ->editColumn('last_name', function (AulaTicCompetitor $competitor) {
                return $competitor->forum_competitor->last_name;
            })
            ->editColumn('dni', function (AulaTicCompetitor $competitor) {
                return $competitor->forum_competitor->dni;
            })
            ->editColumn('curse', function (AulaTicCompetitor $competitor) {
                return $competitor->aula_tic->getCurse();
            })
            ->editColumn('hour', function ($competitor){
                return $competitor->aula_tic->getHour();
            })
            ->editColumn('forum', function ($competitor){
                return $competitor->forum_competitor->forum->city->getCity();
            })
            ->rawColumns(['code', 'curse', 'hour', 'forum'])
            ->make(true);

    }

    public function edit($id)
    {
        $pageTitle = 'Aula TIC';
        $pageIcon = 'fas fa-bullhorn';
        $aulatic = AulaTic::find($id);
        if (empty($aulatic))
        {
            return Redirect::back();
        }

        return view('cpanel.forums.events.aulatic.edit', compact(
            'pageTitle', 'pageIcon', 'aulatic'
        ));
    }

    public function update(Request $request, $id)
    {
        $aulatic = AulaTic::find($id);
        $aulatic->update($request->all());

        toastSuccess('Curso Actualizado');
        return Redirect::route('admin.forum.create.aulatic', $aulatic->forum_id);

    }

    public function destroy($id)
    {
        $data = [];
        $data['success'] = false;

        $aulatic = AulaTic::find($id);
        if (empty($aulatic))
        {
            $data['message'] = 'Error al eliminar AulaTic';
            return $this->ReturnJson($data);
        }
        $aulatic->delete();


        $data['success'] = true;
        $data['message'] = 'AulaTic Eliminada';

        return $this->ReturnJson($data);
    }


}
