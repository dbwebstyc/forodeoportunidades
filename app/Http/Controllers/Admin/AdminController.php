<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserPostRequest;
use App\Http\Requests\UserUpdateRequest;
use App\User;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Administradores del Sistema';
        $pageIcon = 'fas fa-users';
        $users = User::all();

        return view('cpanel.users.index', compact('pageTitle', 'users', 'pageIcon'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Nuevo Usuario Adminsitrador';
        $pageIcon = 'fas fa-users';
        return view('cpanel.users.create', compact('pageTitle', 'pageIcon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserPostRequest $request)
    {
        $user = new User($request->all());
        $user->setPass($request->password);
        $user->save();

        toastSuccess('Usuario creado exitosamente');
        return Redirect::back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pageTitle = 'Editar Usuario';
        $pageIcon = 'fas fa-edit';
        $user = User::findOrFail($id);

        return view('cpanel.users.edit', compact('user', 'pageTitle', 'pageIcon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::find($id);
        $user->setPass($request->password);
        $user->update($request->except('password'));

        toastSuccess('Usuario actualizaco exitosamente');
        return Redirect::route('admin.users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try{
            $user = User::find($id);
            if ($user->id == 1){
                return response()->json([
                    'error' => true,
                    'message' => 'Acción no permitida'
                ], 404);
            }
            $user->delete();
            return response()->json([
                'success' => true,
                'message' => 'Usuario eliminado correctamente.'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'error' => true,
                'message' => 'Intente nuevamente.'
            ], 404);
        }

    }
}
