<?php

namespace App\Http\Controllers\Admin;

use App\Forum;
use App\Http\Controllers\Controller;
use App\WorkShop;
use App\WorkShopCompetitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class WorkShopController extends Controller
{
    public function index()
    {
        $pageTitle = 'Participantes Work Shop';
        $pageIcon = 'fas fa-clipboard';
        $forums = Forum::all();
        $workshops = WorkShop::all();
        $competitors = WorkShopCompetitor::all();

        return view('cpanel.workshops.index', compact(
            'pageIcon', 'pageTitle', 'competitors',
            'forums', 'workshops'
        ));
    }

    public function data(Request $request)
    {
        $competitors = WorkShopCompetitor::join('work_shops', 'work_shop_competitors.work_shop_id', '=', 'work_shops.id');
        if ($request->forum != 'all' && $request->forum != '')
        {
            $competitors = $competitors->where('work_shop_competitors.forum_id', $request->forum);
        }
        if ($request->temary != 'all' && $request->temary != '')
        {
            $competitors = $competitors->where('work_shops.id', $request->temary);
        }

        $competitors = $competitors->get();


        return DataTables::of($competitors)
            ->editColumn('code', function (WorkShopCompetitor $competitor) {
                return $competitor->forum_competitor->code;
            })
            ->editColumn('first_name', function (WorkShopCompetitor $competitor) {
                return $competitor->forum_competitor->first_name;
            })
            ->editColumn('last_name', function (WorkShopCompetitor $competitor) {
                return $competitor->forum_competitor->last_name;
            })
            ->editColumn('dni', function (WorkShopCompetitor $competitor) {
                return $competitor->forum_competitor->dni;
            })
            ->editColumn('hour', function ($competitor){
                return $competitor->WorkShop->getHour();
            })
            ->editColumn('temary', function ($competitor){
                return $competitor->WorkShop->getTemary();
            })
            ->editColumn('forum', function ($competitor){
                return $competitor->WorkShop->forum->city->getCity();
            })
            ->rawColumns(['code', 'hour', 'forum'])
            ->make(true);

    }

    public function indexWorkShop($id)
    {
        $pageTitle = 'Work Shop';
        $pageIcon = 'fas fa-toolbox';
        $forum = Forum::find($id);
        $workshops = WorkShop::where('forum_id', $id)->get();

        return view('cpanel.forums.events.workshop.index', compact(
            'pageTitle', 'pageIcon', 'forum', 'workshops'
        ));
    }

    public function storeWorkShop(Request $request)
    {
        $workshop = new WorkShop($request->all());
        $workshop->setPositionAttribute($request->forum_id);
        $workshop->save();

        toastSuccess('Curso Asignado');
        return Redirect::back();

    }
}
