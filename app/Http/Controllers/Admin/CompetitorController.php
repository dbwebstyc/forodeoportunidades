<?php

namespace App\Http\Controllers\Admin;

use App\Events\ForumRegistered;
use App\Forum;
use App\ForumCompetitor;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompetitorStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class CompetitorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Participantes';
        $pageIcon = 'fas fa-users';
        $forums = Forum::all();
        $competitors = ForumCompetitor::all();

        return view('cpanel.competitors.index', compact(
            'competitors', 'pageTitle', 'pageIcon', 'forums'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Asignar Participante';
        $pageIcon = 'fas fa-user';
        $forums = Forum::all();

        return view('cpanel.competitors.create', compact(
            'pageTitle', 'pageIcon', 'forums'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CompetitorStoreRequest $request)
    {
        $competitor = new ForumCompetitor($request->except(['code']));
        $competitor->code = generateCodeNumber();
        $competitor->save();

        event(new ForumRegistered($competitor));
        toastSuccess('Participante agregado al Foro.');
        return Redirect::route('admin.forum.competitors');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function data(Request $request)
    {


        if ($request->forum != 'all')
        {
            $forums = ForumCompetitor::where('forum_id', $request->forum)->get();
        }else{
            $forums = ForumCompetitor::all();
        }

        return DataTables::of($forums)
            ->editColumn('code', function (ForumCompetitor $forum) {
                return $forum->code;
            })
            ->editColumn('genre', function (ForumCompetitor $forum) {
                return $forum->getGenre();
            })
            ->editColumn('forum', function ($forum){
                return $forum->forum->city->getCity();
            })
            ->rawColumns(['code', 'forum', 'genre'])
            ->make(true);

    }

}
