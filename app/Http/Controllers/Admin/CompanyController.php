<?php

namespace App\Http\Controllers\Admin;

use App\Company;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Lista de Empresas';
        $pageIcon = 'fas fa-user-tie';
        $companies = Company::all();
        return view('cpanel.companies.index', compact('pageTitle', 'pageIcon', 'companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Crear Empresa';
        $pageIcon = 'fas fa-plus-square';

        return view('cpanel.companies.create', compact('pageTitle', 'pageIcon'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CompanyStoreRequest $request)
    {
//        $password = Str::random(8);
        $company = new Company($request->all());
        $company->setPass($request->password);
        $company->save();


        toastSuccess('Empresa creada exitosamente');
        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $company = Company::find($id);
        $pageTitle = $company->name;
        $pageIcon = 'fas fa-user-tie';

        return view('cpanel.companies.show', compact('company', 'pageIcon', 'pageTitle'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        $pageTitle = $company->name;
        $pageIcon = 'fas fa-user-tie';

        return view('cpanel.companies.edit', compact('company', 'pageIcon', 'pageTitle'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(CompanyUpdateRequest $request, $id)
    {
        $company = Company::find($id);
        $company->setPass($request->password);
        $company->update($request->except('password'));

        toastSuccess('Datos de empesa actualizados exitosamente.');
        return Redirect::route('admin.companies');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $company = Company::find($id);
            $company->delete();
            return response()->json([
                'success' => true,
                'message' => 'Empresa eliminada exitosamente.'
            ], 200);
        }catch (\Exception $exception){
            return response()->json([
                'error' => true,
                'message' => 'Error, intente nuevamente'
            ], 403);
        }
    }
}
