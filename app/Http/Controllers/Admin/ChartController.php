<?php

namespace App\Http\Controllers\Admin;

use App\Charts\CompetitorChart;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function chartAges()
    {
        $forum = DB::table('forums')
            ->join('forum_competitors as competitor', 'forums.id', '=', 'competitor.forum_id')
            ->select(DB::raw("DATE_FORMAT(forums.date, '%d %b') as mes, COUNT(competitor.id) as total"))
            ->groupBy('forums.id')
            ->where('forums.status', '=', 1)
            ->get()->toArray();
        $mes = array_column($forum, 'mes');
        $total = array_column($forum, 'total');

//            dd($total);
        $chart = new CompetitorChart();
        $chart->labels($mes);
        $chart->dataset('Edades', 'line', $total);
        return dd($chart);
    }
}
