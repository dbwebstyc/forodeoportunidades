<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomForumController extends Controller
{
    //

public function index(){
 $pageTitle = 'Foros';
        $pageIcon = 'fas fa-home';
        $forums = Forum::all();
 

        return view('cpanel.forums.index', compact('pageIcon', 'pageTitle', 'forums'));
    }

}


