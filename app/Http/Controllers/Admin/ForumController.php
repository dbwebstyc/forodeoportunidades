<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Forum;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForumStoreRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

class ForumController extends Controller
{

    public function index()
    {
        $pageTitle = 'Foros';
        $pageIcon = 'fas fa-home';
        $forums = Forum::all();

        return view('cpanel.forums.index', compact('pageIcon', 'pageTitle', 'forums'));
    }

    public function create()
    {
        $pageTitle = 'Nuevo Foro';
        $pageIcon = 'fas fa-plus';
        $cities = City::Active()->get();
        return view('cpanel.forums.create', compact('pageTitle', 'pageIcon', 'cities'));
    }

    public function store(ForumStoreRequest $request)
    {
        $forum = new Forum($request->except('program_es', 'program_ca'));
        if ($request->hasFile('program_es')) {
            $file = $request->file('program_es');
            $file_extension = $file->getClientOriginalExtension();
            $random_name = Str::slug($file->getClientOriginalName()) . '-' . time();
            $destination_path = public_path().'/uploads/docs/es/';
            $filename = $random_name.'.'.$file_extension;
            $request->file('program_es')->move($destination_path,$filename);

            $forum->program_es = $filename;
        }
        if ($request->hasFile('program_ca')) {
            $file = $request->file('program_ca');
            $file_extension = $file->getClientOriginalExtension();
            $random_name = Str::slug($file->getClientOriginalName()) . '-' . time();
            $destination_path = public_path().'/uploads/docs/ca/';
            $filename = $random_name.'.'.$file_extension;
            $request->file('program_ca')->move($destination_path,$filename);

            $forum->program_ca = $filename;
        }
        $forum->save();

        toastSuccess('Forum creado');
        return Redirect::route('admin.forums');
    }

    public function edit($id)
    {
        $pageTitle = 'Editar Forum';
        $pageIcon = 'fas fa-edit';
        $cities = City::Active()->get();
        $forum = Forum::find($id);

        return view('cpanel.forums.edit', compact('pageIcon', 'pageTitle', 'forum', 'cities'));
    }

    public function update(Request $request, $id)
    {
        $forum = Forum::find($id);
        if ($request->hasFile('program_es')) {
            File::delete(public_path() . '/uploads/docs/es/' . $forum->program_es);
            $file = $request->file('program_es');
            $file_extension = $file->getClientOriginalExtension();
            $random_name = Str::slug($file->getClientOriginalName()) . '-' . time();
            $destination_path = public_path().'/uploads/docs/es/';
            $filename = $random_name.'.'.$file_extension;
            $request->file('program_es')->move($destination_path,$filename);

            $forum->program_es = $filename;
        }
        if ($request->hasFile('program_ca')) {
            File::delete(public_path() . '/uploads/docs/ca/' . $forum->program_ca);
            $file = $request->file('program_ca');
            $file_extension = $file->getClientOriginalExtension();
            $random_name = Str::slug($file->getClientOriginalName()) . '-' . time();
            $destination_path = public_path().'/uploads/docs/ca/';
            $filename = $random_name.'.'.$file_extension;
            $request->file('program_ca')->move($destination_path,$filename);

            $forum->program_ca = $filename;
        };
        $forum->update($request->except('program_es', 'program_ca'));

        toastSuccess('Forum actualizado.');
        return Redirect::route('admin.forums');
    }

    public function data()
    {
        $forums = Forum::all();

        return DataTables::of($forums)
            ->editColumn('created_at', function (Forum $forum) {
                return $forum->created_at;
            })
            ->editColumn('date', function(Forum $forum) {
                return $forum->date->format('dM');
            })
            ->editColumn('city', function (Forum $forum) {
                return $forum->city->getCity();
            })
            ->editColumn('location', function (Forum $forum) {
                return $forum->getLocation();
            })
            ->addColumn('actions', function ($forum) {
                $action = '';
                $action .= '<a title="Editar Foro" href="'.route('admin.forum.edit', $forum->id).'" class="btn btn-info-soft btn-sm mr-1"><i class="far fa-edit"></i></a>';
                $action .= '<a title="Asignar Role Play" href="'.route('admin.forum.create.roleplay', $forum->id).'" class="btn btn-inverse-soft btn-sm mr-1"><i class="far fa-clipboard"></i></a>';
                $action .= '<a title="Asignar Entrevistas" href="'.route('admin.forum.create.interview', $forum->id).'" class="btn btn-inverse-soft btn-sm mr-1"><i class="fas fa-pen-alt"></i></a>';
                $action .= '<a title="Asignar Aula TIC" href="'.route('admin.forum.create.aulatic', $forum->id).'" class="btn btn-inverse-soft btn-sm mr-1"><i class="fas fa-bullhorn"></i></a>';
                $action .= '<a title="Asignar Work Shop" href="'.route('admin.forum.create.workshop', $forum->id).'" class="btn btn-inverse-soft btn-sm mr-1"><i class="fas fa-toolbox"></i></a>';
//                $action .= '<a href="#" onclick="deleteForum('.$forum->id.')" class="btn btn-danger-soft btn-sm "><i class="fas fa-trash"></i></a>';
                $action .= '<a href="#" class="btn btn-danger-soft btn-sm "><i class="fas fa-trash"></i></a>';
                if ($forum->status == true){
//                    $action .= '<a href="#" onclick="disableForum('.$forum->id.')" class="btn btn-danger-soft btn-sm "><i class="fas fa-ban"></i></a>';
                    $action .= '<a href="#" class="btn btn-danger-soft btn-sm "><i class="fas fa-ban"></i></a>';
                }else{
//                    $action .= '<a href="#" onclick="activeForum('.$forum->id.')" class="btn btn-success-soft btn-sm "><i class="fas fa-check-circle"></i></a>';
                    $action .= '<a href="#" class="btn btn-success-soft btn-sm "><i class="fas fa-check-circle"></i></a>';
                }
//                $action .= '<a href="#" onclick="deleteForum('.$forum->id.')" class="btn btn-danger-soft btn-sm "><i class="fas fa-trash"></i></a>';
                return $action;
            })
            ->rawColumns(['actions', 'city', 'location'])
            ->make(true);
    }

    public function destroy(Request $request)
    {
        $data = [];
        $data['success'] = false;

        $id = $request->id;
        $forum  = Forum::find($id);
        if (empty($forum)){
            $data['message'] = 'Error al eliminar este Foro';
            return $this->ReturnJson($data);
        }
        $forum->delete();

        $data['success'] = true;
        $data['message'] = 'Foro Eliminado';
        return $this->ReturnJson($data);
    }

    public function active(Request $request)
    {
        $data = [];
        $data['success'] = false;

        $id = $request->id;
        $forum  = Forum::find($id);
        if (empty($forum)){
            $data['message'] = 'Error al activar este Foro';
            return $this->ReturnJson($data);
        }
        $forum->status = 1;
        $forum->save();

        $data['success'] = true;
        $data['message'] = 'Foro Activado';
        return $this->ReturnJson($data);
    }

    public function disable(Request $request)
    {
        $data = [];
        $data['success'] = false;

        $id = $request->id;
        $forum  = Forum::find($id);
        if (empty($forum)){
            $data['message'] = 'Error al deshabilitar este Foro';
            return $this->ReturnJson($data);
        }
        $forum->status = 0;
        $forum->save();

        $data['success'] = true;
        $data['message'] = 'Foro deshabilitado';
        return $this->ReturnJson($data);
    }
}
