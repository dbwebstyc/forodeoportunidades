<?php

namespace App\Http\Controllers\Admin;

use App\RolePlayCompetitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RolePlayCompetitorController extends Controller
{
    public function index()
    {
        $pageTitle = 'Paticipantes de Role Playing';
        $pageIcon = 'fas fa-user';
        $competitors = RolePlayCompetitor::all();

        return view('cpanel.roleplays.competitors', compact('pageTitle', 'pageIcon', 'competitors'));
    }
}
