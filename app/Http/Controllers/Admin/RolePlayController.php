<?php

namespace App\Http\Controllers\Admin;

use App\Forum;
use App\Hour;
use App\Http\Controllers\Controller;
use App\RolePlay;
use App\RolePlayCompetitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class RolePlayController extends Controller
{
    public function index()
    {
        $pageTitle = 'Role Playing List';
        $pageIcon = 'fas fa-clipboard';
        $forums = Forum::all();
        $competitors = RolePlayCompetitor::all();

        return view('cpanel.roleplays.index', compact(
            'pageIcon', 'pageTitle', 'competitors', 'forums'
        ));
    }

    public function indexRolePlay($id)
    {
        $pageTitle = 'Role Play';
        $pageIcon = 'fas fa-clipboard';
        $forum = Forum::find($id);
        $roleplays = RolePlay::where('forum_id', $id)->get();
        $hours = Hour::all();
        return view('cpanel.forums.events.roleplay.index', compact(
            'pageTitle', 'pageIcon', 'forum', 'roleplays', 'hours'
        ));
    }

    public function storeRolePlay(Request $request)
    {
        $data = array();
        $find = array();
        foreach ($request->hour as $hour) {
            $find = RolePlay::where('hour', $hour)->where('forum_id', $request->forum_id)->first();
            if (empty($find)) {
                $data[] = RolePlay::create([
                    'forum_id' => $request->forum_id,
                    'hour' => $hour,
                    'places' => $request->places
                ]);
            }
        }
        if (!empty($find)) toastWarning('Hay Horas Duplicadas.');
        if (!empty($data)) toastSuccess('Horas Asignadas');
        return Redirect::back();
    }

    public function data(Request $request)
    {


        if ($request->forum != 'all') {
            $competitors = RolePlayCompetitor::where('forum_id', $request->forum)->get();
        } else {
            $competitors = RolePlayCompetitor::all();
        }

        return DataTables::of($competitors)
            ->editColumn('code', function (RolePlayCompetitor $competitor) {
                return $competitor->forum_competitor->code;
            })
            ->editColumn('first_name', function (RolePlayCompetitor $competitor) {
                return $competitor->forum_competitor->first_name;
            })
            ->editColumn('last_name', function (RolePlayCompetitor $competitor) {
                return $competitor->forum_competitor->last_name;
            })
            ->editColumn('dni', function (RolePlayCompetitor $competitor) {
                return $competitor->forum_competitor->dni;
            })
            ->editColumn('hour', function ($competitor) {
                return $competitor->roleplay->hour;
            })
            ->editColumn('forum', function ($competitor) {
                return $competitor->forum_competitor->forum->city->getCity();
            })
            ->rawColumns(['code', 'hour', 'forum'])
            ->make(true);

    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $hour = RolePlay::where($where)->first();

        return response()->json($hour);
    }

    public function update(Request $request)
    {
        $hour =  RolePlay::updateOrCreate(
            ['id' => $request->roleplay_id],
            ['hour' => $request->hour, 'places' => $request->places]
        );

        return response()->json($hour);
    }

    public function destroy($id)
    {
        $data = [];
        $data['success'] = false;

        $id = array('id' => $id);
        if (empty($id))
        {
            $data['message']  = 'Error, intente nuevamente';
            return reponse()->json($data);
        }
        $hour = RolePlay::where($id)->delete();

        $data['success'] = true;
        $data['message'] = 'Hora Eliminada';

        return response()->json($data
        );
    }
}
