<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Company;
use App\CompanyHourInterview;
use App\Forum;
use App\Hour;
use App\Http\Controllers\Controller;
use App\Interview;
use App\InterviewCompetitor;
use App\RolePlayCompetitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;

class InterviewController extends Controller
{
    public function index()
    {
        $pageTitle = 'Entrevistas';
        $pageIcon = 'fas fa-clipboard';
        $forums = Forum::all();
        $companies = Company::all();
        $competitors = InterviewCompetitor::all();

        return view('cpanel.interviews.index', compact(
            'pageIcon', 'pageTitle', 'competitors', 'forums',
            'companies'
        ));
    }

    public function indexInterview($id)
    {
        $pageTitle = 'Entrevistas';
        $pageIcon = 'fas fa-pen-alt';
        $forum = Forum::find($id);
        $interviews = Interview::where('forum_id', $id)->get();
        $hours = Hour::all();
        $companies = Company::all();
        return view('cpanel.forums.events.interviews.index', compact(
            'pageTitle', 'pageIcon', 'forum', 'interviews', 'hours', 'companies'
        ));
    }

    public function storeInterview(Request $request)
    {
        $interview = new Interview();
        $interview->forum_id = $request->forum_id;
        $interview->company_id = $request->company_id;

        if ($interview->save()){
            $data = array();
            $find = array();
            foreach ($request->hour as $hour)
            {
                $find = CompanyHourInterview::where('hour', $hour)->where('company_id', $request->company_id)->where('forum_id', $request->forum_id)->first();
                if (empty($find)){
                    $data[] = CompanyHourInterview::create([
                        'hour' => $hour,
                        'places' => $request->places,
                        'company_id' => $request->company_id,
                        'interview_id' => $interview->id,
                        'forum_id' => $request->forum_id
                    ]);
                }

            }
            if (!empty($find)) {
                Interview::find($interview->id)->delete();
                toastWarning('Hay Horas Duplicadas.');
            }
            if (!empty($data))  toastSuccess('Horas Asignadas');
        }

        return Redirect::back();
    }

    public function data(Request $request)
    {
        $competitors = InterviewCompetitor::join('interviews', 'interview_competitors.interview_id', '=', 'interviews.id');
        if ($request->forum != 'all' && $request->forum != '')
        {
            $competitors = $competitors->where('interviews.forum_id', $request->forum);
        }
        if ($request->company != 'all' && $request->company != '')
        {
            $competitors = $competitors->where('interviews.company_id', $request->company);
        }

        $competitors = $competitors->get();

//        $competitors = InterviewCompetitor::join('interviews', 'interview_competitors.interview_id', '=', 'interviews.id')
//            ->where('interviews.forum_id', $request->forum)
//            ->where('interviews.company_id', $request->company)
//            ->get();
//        if ($request->forum != 'all' || $request->company != 'all')
//        {
//            $competitors = InterviewCompetitor::where('forum_id', $request->forum)->where('company_id', $request->company)->get();
//        }else{
//            $competitors = RolePlayCompetitor::all();
//        }

        return DataTables::of($competitors)
            ->editColumn('code', function (InterviewCompetitor $competitor) {
                return $competitor->forum_competitor->code;
            })
            ->editColumn('first_name', function (InterviewCompetitor $competitor) {
                return $competitor->forum_competitor->first_name;
            })
            ->editColumn('last_name', function (InterviewCompetitor $competitor) {
                return $competitor->forum_competitor->last_name;
            })
            ->editColumn('dni', function (InterviewCompetitor $competitor) {
                return $competitor->forum_competitor->dni;
            })
            ->editColumn('hour', function ($competitor){
                return $competitor->getHour();
            })
            ->editColumn('company', function ($competitor){
                return $competitor->Interview->company->name;
            })
            ->editColumn('forum', function ($competitor){
                return $competitor->Interview->forum->city->getCity();
            })
            ->rawColumns(['code', 'hour', 'forum'])
            ->make(true);

    }

    public function edit($id)
    {
        $where = array('id' => $id);
        $hour = CompanyHourInterview::where($where)->first();

        return response()->json($hour);
    }

    public function update(Request $request)
    {
        $data = [];
        $id = $request->interview_id;
        $hour =  CompanyHourInterview::find($id);
        $hour->hour = $request->hour;
        $hour->places = $request->places;
        $hour->update();

        $data = $hour;
        $data .= $hour->company->name;


        return response()->json($data);
    }

    public function destroy($id)
    {
        $data = [];
        $data['success'] = false;

        $id = array('id' => $id);
        if (empty($id))
        {
            $data['message']  = 'Error, intente nuevamente';
            return reponse()->json($data);
        }
        $hour = CompanyHourInterview::find($id);
        $hour->delete();

        $data['success'] = true;
        $data['message'] = 'Hora Eliminada';

        return response()->json($data);
    }
}
