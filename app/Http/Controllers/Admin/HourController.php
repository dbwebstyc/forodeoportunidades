<?php

namespace App\Http\Controllers\Admin;

use App\Hour;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class HourController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pageTitle = 'Horas';
        $pageIcon = 'fas fa-clock';
        $hours = Hour::all();

        return  view('cpanel.hours.index', compact(
            'pageTitle', 'pageIcon', 'hours'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pageTitle = 'Crear Horas';
        $pageIcon = 'fas fa-clock';

        return view('cpanel.hours.create', compact('pageIcon', 'pageTitle'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $hour = new Hour($request->all());
        $hour->save();

        toastSuccess('Hora Registrada');
        return Redirect::route('admin.hours');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pageTitle = 'Crear Horas';
        $pageIcon = 'fas fa-clock';
        $hour = Hour::find($id);

        return view('cpanel.hours.edit', compact(
            'pageTitle', 'pageIcon', 'hour'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $hour = Hour::find($id);
        $hour->update($request->all());

        toastSuccess('Hora Actualizada');
        return Redirect::route('admin.hours');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
