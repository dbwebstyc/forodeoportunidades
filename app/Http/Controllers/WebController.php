<?php

namespace App\Http\Controllers;

use App\AulaTic;
use App\AulaTicCompetitor;
use App\CompanyHourInterview;
use App\Events\ForumRegistered;
use App\Forum;
use App\ForumCompetitor;
use App\Http\Requests\ForumCompetitorStore;
use App\Interview;
use App\InterviewCompetitor;
use App\Mail\MailInterview;
use App\Mail\MailRolePlay;
use App\Mail\MailWorkShop;
use App\Mail\RegisteredAulaTic;
use App\RolePlay;
use App\RolePlayCompetitor;
use App\WorkShop;
use App\WorkShopCompetitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class WebController extends Controller
{

    public function index(){
        $pageTitle = 'Foro Connecta Labora';
        $forums = Forum::where('status', 1)->orderBy('date', 'desc')->get();
        return view('web.index', compact('forums', 'pageTitle'));
    }

    public function storeCompetitor(ForumCompetitorStore $request)
    {
        $find = ForumCompetitor::where('dni', $request->dni)->where('forum_id', $request->forum_id)->first();
        if (!empty($find)){
            alert()->warning('Alerta!', trans('alertas.ya_registrado_forum'));
            return Redirect::back();
        }
        $competitor = new ForumCompetitor($request->except(['code']));
        $competitor->code = generateCodeNumber();
        $competitor->save();

        event(new ForumRegistered($competitor));
        alert()->success('Exito!', trans('alertas.registrado_en_forum'));
        return back();

    }

    public function rolePlaying($id = null)
    {
        $forumId = $id;
        $pageTitle = 'Role Playing';
        $forums = Forum::Active()->orderBy('date', 'desc')->get();

        return view('web.role_playing',
            compact('forums', 'pageTitle', 'forumId'));
    }

    public function storeRolePlaying(Request $request)
    {
        $roleplay = RolePlay::find($request->hour);
        $forumCompetitor = ForumCompetitor::CheckForumCompetitor($request->dni, $request->forum_id)->first();
        if (empty($forumCompetitor)){
            alert()->info(trans('alertas.titulos.atencion'), trans('alertas.registro_forum_requerido'));
            return Redirect::route('forum');
        }else{

            $rolePlayCompetitor = RolePlayCompetitor::CheckRolePlayCompetitor($forumCompetitor->id, $request->forum_id)->first();
            if (!empty($rolePlayCompetitor)){
                alert()->warning('Alerta!', trans('alertas.ya_registrado_en_evento'));
                return Redirect::back();
            }
        }

        $competitor =  new RolePlayCompetitor();
        $competitor->forum_competitor_id = $forumCompetitor->id;
        $competitor->role_play_id = $roleplay->id;
        $competitor->forum_id = $request->forum_id;

        if ($competitor->save())
        {
            if ($roleplay->places > 0){
                $roleplay->places = $roleplay->places - 1;
            }
            $roleplay->update();
        }

        Mail::to($forumCompetitor->email)->send(new MailRolePlay($competitor));
        alert()->success('Exito!',trans('alertas.registrado_en_evento'));
        return Redirect::back();
    }

    public function getHoursByForum(Request $request)
    {
        if (!$request->forum) {
            $html = '<option value="">Seleccione una Hora</option>';
        } else {
            $html = '<option value="">Seleccione una Hora</option>';
            $hours = RolePlay::where('forum_id', $request->forum)->where('places', '>', 0)->orderBy('hour', 'asc')->get();
            foreach ($hours as $hour) {
                $html .= '<option value="'.$hour->id.'">'.$hour->getHourRolePlay(). ' Plazas: '. $hour->places.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function interview($id = null)
    {
        $forumId = $id;
        $pageTitle = 'Entrevistas de Trabajo';
        $forums = Forum::Active()->orderBy('date', 'desc')->get();

        return view('web.interview',
            compact('forums', 'pageTitle', 'forumId'));
    }

    public function storeInterview(Request $request)
    {
        $interview = Interview::where('forum_id', $request->forum_id)->where('company_id', $request->company)->first();
        $forumCompetitor = ForumCompetitor::CheckForumCompetitor($request->dni, $request->forum_id)->first();
        if (empty($forumCompetitor)){
            alert()->info(trans('alertas.titulos.atencion'), trans('alertas.registro_forum_requerido'));
            return Redirect::route('forum');
        }else{

            $interviewCompetitor = InterviewCompetitor::CheckCompetitor($forumCompetitor->id, $request->company, $interview->id, $request->hour)->first();
            if (!empty($interviewCompetitor)){
                alert()->warning('Alerta!', trans('alertas.ya_registrado_en_evento'));
                return Redirect::back();
            }
        }

        $competitor =  new InterviewCompetitor();
        $competitor->forum_competitor_id = $forumCompetitor->id;
        $competitor->hour = $request->hour;
        $competitor->interview_id = $interview->id;
        $competitor->forum_id = $request->forum_id;

        if ($competitor->save())
        {
            $companyInterview = CompanyHourInterview::where('company_id', $request->company)->where('interview_id', $interview->id)->where('hour', $request->hour)->first();
            if ($companyInterview->places > 0)
            {
                $companyInterview->places = $companyInterview->places - 1;
            }
            $companyInterview->update();
        }


        Mail::to($forumCompetitor->email)->send(new MailInterview($competitor));
        alert()->success('Exito!',trans('alertas.registrado_en_evento'));
        return Redirect::back();
    }

    public function getCompanyByInterview(Request $request)
    {
        if (!$request->forum) {
            $html = '<option value="">Seleccionar Empresa</option>';
        } else {
            $html = '<option value="">Seleccionar Empresa</option>';
            $companies = Interview::where('forum_id', $request->forum)->orderBy('company_id', 'desc')->get();
            foreach ($companies as $company) {
                $html .= '<option value="'.$company->company_id.'">'.$company->company->name.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function getHoursByInterview(Request $request)
    {
        if (!$request->company && !$request->forum) {
            $html = '<option value="">Seleccione una Hora</option>';
        } else {
            $html = '<option value="">Seleccione una Hora</option>';
            $hours = CompanyHourInterview::where('forum_id', $request->forum)->where('company_id', $request->company)->where('places', '>', 0)->orderBy('hour', 'asc')->get();
            foreach ($hours as $hour) {
                $html .= '<option value="'.$hour->hour.'">'.$hour->hour. ' Plazas: '. $hour->places.'</option>';
            }
        }

        return response()->json(['html' => $html]);
    }

    public function aulatic($id = null)
    {
        $forumId = $id;
        $pageTitle = 'Aula TIC';
        $forums = Forum::Active()->orderBy('date', 'desc')->get();

        return view('web.aulatic',
            compact('forums', 'pageTitle', 'forumId'));
    }

    public function storeAulaTic(Request $request)
    {
        $aula = AulaTic::find($request->curse);
        $forumCompetitor = ForumCompetitor::CheckForumCompetitor($request->dni, $request->forum_id)->first();
        if (empty($forumCompetitor)){
            alert()->info(trans('alertas.titulos.atencion'), trans('alertas.registro_forum_requerido'));
            return Redirect::route('forum');
        }else{

            $aulaCompetitor = AulaTicCompetitor::CheckCompetitor($forumCompetitor->id, $request->forum_id)->first();
            if (!empty($aulaCompetitor)){
                alert()->warning('Alerta!', trans('alertas.ya_registrado_en_evento'));
                return Redirect::back();
            }
        }

        $competitor =  new AulaTicCompetitor();
        $competitor->forum_competitor_id = $forumCompetitor->id;
        $competitor->aula_tic_id = $aula->id;
        $competitor->forum_id = $request->forum_id;

        if ($competitor->save())
        {
            if ($aula->places > 0)
            {
                $aula->places = $aula->places - 1;
            }
            $aula->update();
        }
        Mail::to($forumCompetitor->email)->send(new RegisteredAulaTic($competitor));
        alert()->success('Exito!',trans('alertas.registrado_en_evento'));
        return Redirect::back();
    }

    public function getCursesByForum(Request $request)
    {
        if (!$request->forum) {
            $html = '<option value="">Seleccione Curso</option>';
        } else {
            $html = '<option value="">Seleccione una Hora</option>';
            $curses = AulaTic::where('forum_id', $request->forum)->where('places', '>', 0)->orderBy('curse_' . app()->getLocale(), 'desc')->get();
            foreach ($curses as $curse) {
                $html .= '<option value="' . $curse->id . '">' . $curse->getCurse() . ' | ' . $curse->hour . ' Plazas: ' . $curse->places . '</option>';
            }
        }
        return response()->json(['html' => $html]);
    }

    public function workshop($id = null)
    {
        $forumId = $id;
        $pageTitle = 'Work Shop';
        $forums = Forum::Active()->orderBy('date', 'desc')->get();

        return view('web.workshop',
            compact('forums', 'pageTitle', 'forumId'));
    }

    public function storeWorkshop(Request $request)
    {
        $workshop = WorkShop::find($request->workshop);
        $forumCompetitor = ForumCompetitor::CheckForumCompetitor($request->dni, $request->forum_id)->first();
        if (empty($forumCompetitor)){
            alert()->info(trans('alertas.titulos.atencion'), trans('alertas.registro_forum_requerido'));
            return Redirect::route('forum');
        }else{

            $workshopCompetitor = WorkShopCompetitor::CheckWorkShopCompetitor($forumCompetitor->id, $request->forum_id)->first();
            if (!empty($workshopCompetitor)){
                alert()->warning('Alerta!', trans('alertas.ya_registrado_en_evento'));
                return Redirect::back();
            }
        }

        $competitor =  new WorkShopCompetitor();
        $competitor->forum_competitor_id = $forumCompetitor->id;
        $competitor->work_shop_id = $workshop->id;
        $competitor->forum_id = $request->forum_id;

        if ($competitor->save())
        {
            if ($workshop->places > 0){
                $workshop->places = $workshop->places - 1;
            }
            $workshop->update();
        }
        Mail::to($forumCompetitor->email)->send(new MailWorkShop($competitor));
        alert()->success('Exito!',trans('alertas.registrado_en_evento'));
        return Redirect::back();
    }

    public function getWorkShops(Request $request)
    {
        if (!$request->forum) {
            $html = '<option value="">Seleccione WorkShop</option>';
        } else {
            $html = '<option value="">Seleccione WorkShop</option>';
            $workshops = WorkShop::where('forum_id', $request->forum)->where('places', '>', 0)->orderBy('temary_' . app()->getLocale(), 'desc')->get();
            foreach ($workshops as $workshop) {
                $html .= '<option value="' . $workshop->id . '">' . $workshop->getTemary() . ' | Hora: ' . $workshop->hour . ' | Plazas: ' . $workshop->places . '</option>';
            }
        }
        return response()->json(['html' => $html]);
    }
}
