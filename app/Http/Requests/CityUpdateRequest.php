<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_es' => 'required|string|min:3|unique:cities,city_es,'.$this->id,
            'city_ca' => 'required|string|min:3|unique:cities,city_es,'.$this->id,
        ];
    }

    public function attributes()
    {
        return [
            'city_es' => 'Nombre de Ciudad Español',
            'city_ca' => 'Nombre de Ciudad Valenciano'
        ];
    }
}
