<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForumCompetitorStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forum_id' => 'required',
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'dni' => 'required|string|min:4',
            'genre' => 'required|string',
            'age' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string|min:8',
        ];
    }

    public function attributes()
    {
        return [
            'forum_id' => 'Foro',
            'first_name' => 'Nombre',
            'last_name' => 'Apellido',
            'dni' => 'DNI',
            'email' => 'Correo Electronico',
            'genre' => 'Genero',
            'age' => 'Edad',
        ];
    }
}
