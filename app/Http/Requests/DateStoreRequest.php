<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DateStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_forum' => 'required|date|unique:forum_dates',
        ];
    }

    public function attributes()
    {
        return [
            'date_forum' => 'Fecha del Foro'
        ];
    }

}
