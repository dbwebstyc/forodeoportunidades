<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'city_es' => 'required|string|min:3|max:50|unique:cities',
            'city_ca' => 'required|string|min:3|max:50|unique:cities'
        ];
    }

    public function attributes()
    {
        return [
          'city_es' => 'Nombre de Ciudad Español',
          'city_ca' => 'Nombre de Ciudad Valenciano'
        ];
    }
}
