<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ForumStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'city_id' => 'required',
            'location_es' => 'required|string|min:3',
            'location_ca' => 'required|string|min:3',
            'program_es' => 'nullable|file|mimes:pdf',
            'program_ca' => 'nullable|file|mimes:pdf'
        ];
    }

    public function attributes()
    {
        return [
            'date' => 'Fecha',
            'city_id' => 'Ciudad',
            'location_es' => 'Nombre Ubicación Español',
            'location_ca' => 'Nombre Ubicación Valenciano',
            'program_es' => 'Programa o Itinerario Español',
            'program_ca' => 'Programa o Itinerario Valenciano'
        ];
    }
}
