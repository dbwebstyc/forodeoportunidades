<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompetitorStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'forum_id' => 'required',
            'first_name' => 'required|string|min:3',
            'last_name' => 'required|string|min:3',
            'dni' => 'required|string',
            'genre' => 'required|string',
            'age' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string'
        ];
    }

    public function attributes()
    {
        return [
            'forum_id' => 'Foro',
            'first_name' => 'Nombre',
            'last_name' => 'Apellidos',
            'dni' => 'DNI',
            'email' => 'Correo Electrónico',
            'phone' => 'Telefono',
            'genre' => 'Genero',
            'age' => 'Edad',
        ];
    }
}
