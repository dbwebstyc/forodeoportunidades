<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class AulaTic extends Model
{
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    // relation
    public function forum()
    {
        return $this->belongsTo(Forum::class);
    }

    public function aulatic_competitor()
    {
        return $this->hasMany(AulaTicCompetitor::class);
    }

    public function getHour()
    {
        $hour = Carbon::parse($this->hour);
        return $hour->format('H:i');
    }

    public function getCurse()
    {
        $locale = app()->getLocale();
        $value = 'curse_' . $locale;
        return $this->{$value};
    }
}
