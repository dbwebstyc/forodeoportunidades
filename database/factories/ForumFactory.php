<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\City;
use App\Forum;
use Faker\Generator as Faker;

$factory->define(Forum::class, function (Faker $faker) {
    return [
        'date' => $faker->date('Y-m-d'),
        'city_id' => $faker->randomElement(),
        'location_es' => $faker->streetAddress,
        'location_ca' => $faker->streetAddress
    ];
});

