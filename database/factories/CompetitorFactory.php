<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Forum;
use App\ForumCompetitor;
use Faker\Generator as Faker;

$factory->define(ForumCompetitor::class, function (Faker $faker) {
    $forums = Forum::orderBy('id', 'DESC')->pluck('id');
    return [
        'code' => $faker->numberBetween(1000, 9000),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'dni' => $faker->randomNumber(8),
        'age' => $faker->numberBetween(18, 70),
        'genre' => $faker->title('M|H'),
        'email' => $faker->email,
        'phone' => $faker->phoneNumber,
        'forum_id' => $faker->ran
    ];
});
