<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hour;
use Faker\Generator as Faker;

$factory->define(Hour::class, function (Faker $faker) {
    return [
        'hour_name' => $faker->time($format = 'H:i'),
        'status' => $faker->boolean()
    ];
});
