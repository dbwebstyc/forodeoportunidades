<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\City;
use Faker\Generator as Faker;

$factory->define(City::class, function (Faker $faker) {
    return [
        'city_es' => $faker->city,
        'city_ca' => $faker->city,
        'status' => 1
    ];
});
