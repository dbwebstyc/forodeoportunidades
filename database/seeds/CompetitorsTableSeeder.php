<?php

use App\Forum;
use App\ForumCompetitor;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CompetitorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
//        factory(App\ForumCompetitor::class, 200)->create();
        $faker = Faker::create();
        $forums = Forum::all()->pluck('id');
        $age = array('16-29', '30-44', '45-59', '+59');
        $genre = array('H', 'M');
        foreach (range(1,300) as $index)
        {
            $competitors = ForumCompetitor::create([
                'code' => $faker->numberBetween(1000, 9000),
                'first_name' => $faker->firstName,
                'last_name' => $faker->lastName,
                'dni' => $faker->randomNumber(8),
                'age' => $faker->randomElement($age),
                'genre' => $faker->randomElement($genre),
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'forum_id' => $faker->randomElement($forums)
            ]);
        }
    }
}
