<?php

use App\City;
use App\Forum;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ForumsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        $cities = City::all()->pluck('id');
        foreach (range(1,3) as $index){
            $forum = Forum::create([
                'date' => $faker->date('Y-m-d'),
                'city_id' => $faker->randomElement($cities),
                'location_es' => $faker->streetAddress,
                'location_ca' => $faker->streetAddress
            ]);
        }
    }
}
