<?php

use Illuminate\Database\Seeder;

class HoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Hour::class, 5)->create()->each(function ($hour) {
            $hour = factory(App\Hour::class)->make();
        });
    }
}
