<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolePlayCompetitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_play_competitors', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->string('code');
//            $table->string('dni');
            $table->unsignedBigInteger('forum_competitor_id');
            $table->boolean('assistance')->default(0); // 0: no 1: si
            $table->bigInteger('role_play_id')->unsigned()->index();
            $table->bigInteger('forum_id')->unsigned()->index();
            $table->foreign('forum_competitor_id')->references('id')->on('forum_competitors');
            $table->foreign('role_play_id')->references('id')->on('role_plays');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_play_competitors');
    }
}
