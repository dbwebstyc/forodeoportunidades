<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyHourInterviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_hour_interviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->time('hour');
            $table->float('places', 10, 0);
            $table->boolean('status')->default(1);
            $table->unsignedBigInteger('company_id');
            $table->unsignedBigInteger('interview_id');
            $table->unsignedBigInteger('forum_id');
            $table->foreign('company_id')->references('id')->on('companies');
            $table->foreign('interview_id')->references('id')->on('interviews');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_hour_interviews');
    }
}
