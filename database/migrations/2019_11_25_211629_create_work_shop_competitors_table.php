<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkShopCompetitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_shop_competitors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('forum_competitor_id');
            $table->unsignedBigInteger('work_shop_id');
            $table->unsignedBigInteger('forum_id');
            $table->foreign('forum_competitor_id')->references('id')->on('forum_competitors');
            $table->foreign('work_shop_id')->references('id')->on('work_shops');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_shop_competitors');
    }
}
