<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulaTicCompetitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aula_tic_competitors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('forum_competitor_id');
            $table->unsignedBigInteger('aula_tic_id');
            $table->unsignedBigInteger('forum_id');
            $table->foreign('forum_competitor_id')->references('id')->on('forum_competitors');
            $table->foreign('aula_tic_id')->references('id')->on('aula_tics');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aula_tic_competitors');
    }
}
