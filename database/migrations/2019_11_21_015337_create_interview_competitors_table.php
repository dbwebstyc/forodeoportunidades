<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewCompetitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interview_competitors', function (Blueprint $table) {
            $table->bigIncrements('id');
//            $table->string('code');
//            $table->string('dni');
            $table->unsignedBigInteger('forum_competitor_id');
            $table->time('hour');
            $table->boolean('assistance')->default(0); // 0: no 1: si
            $table->bigInteger('interview_id')->unsigned()->index();
            $table->bigInteger('forum_id')->unsigned()->index();
            $table->foreign('forum_competitor_id')->references('id')->on('forum_competitors');
            $table->foreign('interview_id')->references('id')->on('interviews');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('interview_competitors');
    }
}
