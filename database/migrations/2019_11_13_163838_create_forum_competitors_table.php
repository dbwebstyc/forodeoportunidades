<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForumCompetitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forum_competitors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('code');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('dni');
            $table->string('age')->nullable();
            $table->string('genre')->nullable();
            $table->string('email');
            $table->string('phone');
            $table->string('school')->nullable();
            $table->string('company')->nullable();
            $table->string('position')->nullable();
            $table->unsignedInteger('number_employees')->nullable();
            $table->string('students_fp')->default(0); //0:no | 1:si
            $table->string('event_interest')->nullable();
            $table->unsignedBigInteger('forum_id');
            $table->foreign('forum_id')->references('id')->on('forums');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('forum_competitors');
    }
}
